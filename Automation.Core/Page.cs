﻿using Automation.Configuration;
using Automation.Core.Exceptions;
using NLog;

namespace Automation.Core
{
    /// <summary>
    /// Class that implement base page functionality.
    /// </summary>
    public abstract class Page
    {
        #region Constants

        protected const string PageNodeType = "Page";
        protected const string ControlsHeadersNodeType = "Header";
        protected const string PageConfigFile = @"./ConfigFiles/PagesControlsData.xml";
        protected const string PageConfigFileSchema = @"./ConfigFiles/PagesControlsSchema.xsd";

        #endregion

        #region Properties

        /// <summary>
        /// Gets the configuration of the application
        /// </summary>
        public static ConfigItems Configuration
        {
            get
            {
                Config.Load();
                return Config.AppSettings;
            }
        }

        /// <summary>
        /// Gets or sets the page timeout.
        /// </summary>
        /// <value>
        /// The page timeout.
        /// </value>
        public int PageTimeout { get; set; }

        /// <summary>
        /// Gets or sets the page information.
        /// </summary>
        /// <value>
        /// The page information.
        /// </value>
        public PageDetails PageInformation { get; protected set; }


        /// <summary>
        /// Gets or sets the name of the page as string. Do not use any fields from derived class to return this value.
        /// </summary>
        /// <value>
        /// The name of the page.
        /// </value>
        public string PageName { get; protected set; }

        public ControlHeaders ControlHeaders { get; protected set; }

        #endregion

        /// <summary>
        /// Opens the page specified by T as user.
        /// </summary>
        /// <typeparam name="T">Type of page.</typeparam>
        /// <param name="user">The user.</param>
        /// <returns>Opened page object.</returns>
        public static T Open<T>() where T : Page, new()
        {
            var page = new T();
            LogManager.GetCurrentClassLogger().Info("Opening page {0}.", page.PageInformation.Url);
            WebBrowser.WebDriver.NavigateToUrl(page.PageInformation.Url);

            return page;
        }

        /// <summary>
        /// Waits for page to load.
        /// </summary>
        public void WaitForLoad()
        {
            if (BasicHelper.WaitForSuccess(() => WebBrowser.WebDriver.ExecuteScript("return document.readyState").Equals("complete"), PageTimeout))
                return;

            throw new PageLoadTimeoutException(string.Format("Wait time for opening page '{0}' exceeded.", PageInformation.Name));
        }
    }
}