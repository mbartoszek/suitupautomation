using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public class OutfitBuilderPageControls : ControlContainer
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OutfitBuilderPageControls" /> class.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="defaultTimeout">The default timeout.</param>
        public OutfitBuilderPageControls(Dictionary<string, ControlDetail> controlDetails, int defaultTimeout)
        {
            DefaultTimeout = defaultTimeout;
            ControlDetails = controlDetails;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        protected Dictionary<string, ControlDetail> ControlDetails { get; private set; }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divSuitsContent
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divSuitsContent"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divWaistcoatsContent
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divWaistcoatsContent"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divNeckwearContent
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divNeckwearContent"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divShirtsContent
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divShirtsContent"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divAccessoriesContent
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divAccessoriesContent"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divItemContainer
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divItemContainer"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divItemContainerWaistcoats
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divItemContainerWaistcoats"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divItemContainerNeckwear
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divItemContainerNeckwear"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divMatchingNeckwear
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divMatchingNeckwear"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divColourContainerNeckwear
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divColourContainerNeckwear"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersSuits
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersSuits"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersWaistcoats
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersWaistcoats"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersNeckwear
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersNeckwear"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersShirts
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersShirts"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersAccessories
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersAccessories"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divClearFilter
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divClearFilter"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersColours
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersColours"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divFiltersStyles
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divFiltersStyles"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewOutfitSummary
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewOutfitSummary"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div GreyColourOfSuit
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["GreyColourOfSuit"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div selectedRuche
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["selectedRuche"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgBlackHerringboneTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgBlackHerringboneTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgGreyStripeTrousers
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgGreyStripeTrousers"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgLovesFirstBlushRuche
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgLovesFirstBlushRuche"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgDoveGreyDoubleBreastedWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgDoveGreyDoubleBreastedWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgGreySilkTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgGreySilkTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgGreySilkTrousers
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgGreySilkTrousers"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgPistachioTie
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgPistachioTie"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgPistachioWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgPistachioWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgBlackSlimFitShort
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgBlackSlimFitShort"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgBlackSlimFitTrousers
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgBlackSlimFitTrousers"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgPurpleStormTie
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgPurpleStormTie"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgPurpleStormWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgPurpleStormWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgNavyHerringbonePrinceEdward
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgNavyHerringbonePrinceEdward"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgNavyStripeTrousers
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgNavyStripeTrousers"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgPomegranateRuche
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgPomegranateRuche"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgPomegranateWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgPomegranateWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgMidGreyTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgMidGreyTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgMidGreyTrousers
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgMidGreyTrousers"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgMidGreyWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgMidGreyWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgNavyRuche
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgNavyRuche"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgGreySilkShort
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgGreySilkShort"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgFuschiaTie
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgFuschiaTie"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgGreySilkWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgGreySilkWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgClaretTie
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgClaretTie"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgBlackSlimFitWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgBlackSlimFitWaistcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgSlateGreyPrinceEdward
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgSlateGreyPrinceEdward"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgSlateGreyTrousers
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgSlateGreyTrousers"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgBerryRuche
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgBerryRuche"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Image imgIvoryWaistcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Image>(ControlDetails["imgIvoryWaistcoat"], GetControlTimeout(5000));
			}
        }
		
		#endregion
	}
}
