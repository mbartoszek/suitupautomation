using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public class MainPageControls : ControlContainer
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MainPageControls" /> class.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="defaultTimeout">The default timeout.</param>
        public MainPageControls(Dictionary<string, ControlDetail> controlDetails, int defaultTimeout)
        {
            DefaultTimeout = defaultTimeout;
            ControlDetails = controlDetails;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        protected Dictionary<string, ControlDetail> ControlDetails { get; private set; }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkSignIn
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkSignIn"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Textbox tbxUsername
        {
            get
            { 
				return ControlFactory.CreateControl<Textbox>(ControlDetails["tbxUsername"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Textbox tbxPassword
        {
            get
            { 
				return ControlFactory.CreateControl<Textbox>(ControlDetails["tbxPassword"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnLoginIn
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnLoginIn"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkForgottenPassword
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkForgottenPassword"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkClose
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkClose"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divUsernameValidation
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divUsernameValidation"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divPasswordValidation
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divPasswordValidation"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkLogout
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkLogout"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkOutfitBuilder
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkOutfitBuilder"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkInspireMe
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkInspireMe"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkHowItWorks
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkHowItWorks"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkOutfitBuilderFromBurger
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkOutfitBuilderFromBurger"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnSubscribe
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnSubscribe"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkTermsConditions
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkTermsConditions"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkCookiesPrivacy
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkCookiesPrivacy"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkHelpFAQs
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkHelpFAQs"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkPriceList
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkPriceList"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkXedo
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkXedo"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnNavigation
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnNavigation"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button spanPauseCarousel
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["spanPauseCarousel"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button spanPlayCarousel
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["spanPlayCarousel"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divCarousel
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divCarousel"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divCarouselIndicators
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divCarouselIndicators"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkViewOutfitsInFiftyShadesOfGrey
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkViewOutfitsInFiftyShadesOfGrey"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewOutfitsInFiftyShadesOfGrey
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewOutfitsInFiftyShadesOfGrey"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkViewOutfitsInColourMeBold
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkViewOutfitsInColourMeBold"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewOutfitsInColourMeBold
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewOutfitsInColourMeBold"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnGetStarted
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnGetStarted"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnFindOutMore
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnFindOutMore"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkBlackTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkBlackTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnBlackTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnBlackTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkGreySilkTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkGreySilkTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnGreySilkTailcoat
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnGreySilkTailcoat"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkBlackSlimFit
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkBlackSlimFit"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnBlackSlimFit
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnBlackSlimFit"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkNavyPrinceEdward
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkNavyPrinceEdward"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnNavyPrinceEdward
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnNavyPrinceEdward"], GetControlTimeout(5000));
			}
        }
		
		#endregion
	}
}
