using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public partial class OrdersPage : NavigationPage
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersPage" /> class.
        /// </summary>
        public OrdersPage()
        {   
            PageName = "OrdersPage";
            PageInformation = XmlHelper.DeserializeFromXml<PageDetails>(XmlHelper.GetXmlDocumentPart(PageNodeType, PageName, PageConfigFile, PageConfigFileSchema));
            PageTimeout = TimeoutHelper.GetTimeout(PageConfigFile, PageConfigFileSchema, PageNodeType);
            Controls = new OrdersPageControls(PageInformation.ControlDetails, PageTimeout);
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets the controls.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>
        protected new OrdersPageControls Controls { get; set; }

        #endregion
    }
}
