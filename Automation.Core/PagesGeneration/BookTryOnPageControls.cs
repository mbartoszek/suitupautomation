using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public class BookTryOnPageControls : ControlContainer
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="BookTryOnPageControls" /> class.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="defaultTimeout">The default timeout.</param>
        public BookTryOnPageControls(Dictionary<string, ControlDetail> controlDetails, int defaultTimeout)
        {
            DefaultTimeout = defaultTimeout;
            ControlDetails = controlDetails;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        protected Dictionary<string, ControlDetail> ControlDetails { get; private set; }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnGetStartedTryOn
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnGetStartedTryOn"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnGetStartedOutfitBuilder
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnGetStartedOutfitBuilder"], GetControlTimeout(5000));
			}
        }
		
		#endregion
	}
}
