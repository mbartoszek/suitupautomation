using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public partial class AccountPage : NavigationPage
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountPage" /> class.
        /// </summary>
        public AccountPage()
        {   
            PageName = "AccountPage";
            PageInformation = XmlHelper.DeserializeFromXml<PageDetails>(XmlHelper.GetXmlDocumentPart(PageNodeType, PageName, PageConfigFile, PageConfigFileSchema));
            PageTimeout = TimeoutHelper.GetTimeout(PageConfigFile, PageConfigFileSchema, PageNodeType);
            Controls = new AccountPageControls(PageInformation.ControlDetails, PageTimeout);
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets the controls.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>
        protected new AccountPageControls Controls { get; set; }

        #endregion
    }
}
