using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public partial class OutfitBuilderPage : NavigationPage
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OutfitBuilderPage" /> class.
        /// </summary>
        public OutfitBuilderPage()
        {   
            PageName = "OutfitBuilderPage";
            PageInformation = XmlHelper.DeserializeFromXml<PageDetails>(XmlHelper.GetXmlDocumentPart(PageNodeType, PageName, PageConfigFile, PageConfigFileSchema));
            PageTimeout = TimeoutHelper.GetTimeout(PageConfigFile, PageConfigFileSchema, PageNodeType);
            Controls = new OutfitBuilderPageControls(PageInformation.ControlDetails, PageTimeout);
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets the controls.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>
        protected new OutfitBuilderPageControls Controls { get; set; }

        #endregion
    }
}
