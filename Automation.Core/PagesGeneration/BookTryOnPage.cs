using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public partial class BookTryOnPage : NavigationPage
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="BookTryOnPage" /> class.
        /// </summary>
        public BookTryOnPage()
        {   
            PageName = "BookTryOnPage";
            PageInformation = XmlHelper.DeserializeFromXml<PageDetails>(XmlHelper.GetXmlDocumentPart(PageNodeType, PageName, PageConfigFile, PageConfigFileSchema));
            PageTimeout = TimeoutHelper.GetTimeout(PageConfigFile, PageConfigFileSchema, PageNodeType);
            Controls = new BookTryOnPageControls(PageInformation.ControlDetails, PageTimeout);
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets the controls.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>
        protected new BookTryOnPageControls Controls { get; set; }

        #endregion
    }
}
