using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public class NavigationPageControls : ControlContainer
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationPageControls" /> class.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="defaultTimeout">The default timeout.</param>
        public NavigationPageControls(Dictionary<string, ControlDetail> controlDetails, int defaultTimeout)
        {
            DefaultTimeout = defaultTimeout;
            ControlDetails = controlDetails;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        protected Dictionary<string, ControlDetail> ControlDetails { get; private set; }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnNavigation
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnNavigation"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkHomeOption
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkHomeOption"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkOutfitBuilderOption
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkOutfitBuilderOption"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkInspireMeOption
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkInspireMeOption"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Link lnkProfileOption
        {
            get
            { 
				return ControlFactory.CreateControl<Link>(ControlDetails["lnkProfileOption"], GetControlTimeout(5000));
			}
        }
		
		#endregion
	}
}
