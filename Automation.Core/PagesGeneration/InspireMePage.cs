using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public partial class InspireMePage : NavigationPage
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="InspireMePage" /> class.
        /// </summary>
        public InspireMePage()
        {   
            PageName = "InspireMePage";
            PageInformation = XmlHelper.DeserializeFromXml<PageDetails>(XmlHelper.GetXmlDocumentPart(PageNodeType, PageName, PageConfigFile, PageConfigFileSchema));
            PageTimeout = TimeoutHelper.GetTimeout(PageConfigFile, PageConfigFileSchema, PageNodeType);
            Controls = new InspireMePageControls(PageInformation.ControlDetails, PageTimeout);
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets the controls.
        /// </summary>
        /// <value>
        /// The controls.
        /// </value>
        protected new InspireMePageControls Controls { get; set; }

        #endregion
    }
}
