using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public class InspireMePageControls : ControlContainer
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="InspireMePageControls" /> class.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="defaultTimeout">The default timeout.</param>
        public InspireMePageControls(Dictionary<string, ControlDetail> controlDetails, int defaultTimeout)
        {
            DefaultTimeout = defaultTimeout;
            ControlDetails = controlDetails;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        protected Dictionary<string, ControlDetail> ControlDetails { get; private set; }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divCarouselInspire
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divCarouselInspire"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Div divCarouselInspireIndicators
        {
            get
            { 
				return ControlFactory.CreateControl<Div>(ControlDetails["divCarouselInspireIndicators"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewInOutfitBuilder1
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewInOutfitBuilder1"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewInOutfitBuilder2
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewInOutfitBuilder2"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewInOutfitBuilder3
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewInOutfitBuilder3"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewInOutfitBuilder4
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewInOutfitBuilder4"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewInOutfitBuilder5
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewInOutfitBuilder5"], GetControlTimeout(5000));
			}
        }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnViewInOutfitBuilder6
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnViewInOutfitBuilder6"], GetControlTimeout(5000));
			}
        }
		
		#endregion
	}
}
