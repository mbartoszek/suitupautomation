using System.Collections.Generic;
using Automation.Core.Controls;
using Automation.Configuration;

namespace Automation.Core.Pages
{
    public class OrdersPageControls : ControlContainer
    {
	    #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersPageControls" /> class.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="defaultTimeout">The default timeout.</param>
        public OrdersPageControls(Dictionary<string, ControlDetail> controlDetails, int defaultTimeout)
        {
            DefaultTimeout = defaultTimeout;
            ControlDetails = controlDetails;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        protected Dictionary<string, ControlDetail> ControlDetails { get; private set; }

        /// <summary>
        /// Gets the controls in page.
        /// </summary>
        /// <value>
        /// The controls in page.
        /// </value>
        public Button btnGoToOrder
        {
            get
            { 
				return ControlFactory.CreateControl<Button>(ControlDetails["btnGoToOrder"], GetControlTimeout(5000));
			}
        }
		
		#endregion
	}
}
