﻿using NLog;
using OpenQA.Selenium;
using System.Drawing.Imaging;
using System.IO;

namespace Automation.Core
{
    public static class WebBrowser
    {
        #region Properties

        /// <summary>
        /// Gets the IWebDriver.
        /// </summary>
        public static MyWebDriver WebDriver { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Open default web browser (defined in config file) and initialize web driver object.
        /// </summary>
        public static void Open(string browser)
        {
            WebDriver = new MyWebDriver(browser);
        }

        /// <summary>
        /// Function that takes screenshot of web browser and save it.
        /// </summary>
        /// <param name="fileName">Name of file with screenshot.</param>
        /// <param name="filePath">Path where screenshot will be saved.</param>
        public static void TakeScreenshot(string fileName, string filePath)
        {
            var screenshotDriver = WebDriver.WebDriver as ITakesScreenshot;
            if (screenshotDriver == null) return;
            var screenshot = screenshotDriver.GetScreenshot();
            var path = Path.GetFullPath(string.Format("{0}\\{1}.png", filePath, fileName.Replace(':', '-')));
            screenshot.SaveAsFile(path, ImageFormat.Png);
        }

        /// <summary>
        /// Closing web browser.
        /// </summary>
        public static void Close()
        {
            if (WebDriver != null)
            {
                WebDriver.Quit();
                LogManager.GetCurrentClassLogger().Info("Web browser is closed.");
                return;
            }

            LogManager.GetCurrentClassLogger().Warn("There is no opened web browser, so cannot be closed.");
        }

        #endregion
    }
}
