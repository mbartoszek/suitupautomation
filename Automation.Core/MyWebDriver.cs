﻿using System.Collections.Generic;
using Automation.Core.Exceptions;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using Automation.Configuration;

namespace Automation.Core
{
    public class MyWebDriver : IDisposable
    {
        #region Constants

        private static readonly TimeSpan PageLoadTimeout = TimeSpan.FromMilliseconds(Config.AppSettings.DefaultPageLoadTimeout);
        private static readonly TimeSpan ImplicitlyWaitTime = TimeSpan.FromMilliseconds(Config.AppSettings.DefaultImplicitlyWait);
        private static readonly TimeSpan FindElementTimeout = TimeSpan.FromMilliseconds(Config.AppSettings.DefaultFindElementTimeout);
        private const string FIREFOX = "FIREFOX";
        private const string CHROME = "CHROME";
        private const string INTERNETEXPLORER = "INTERNETEXPLORER";

        #endregion

        #region Fields

        private readonly IWebDriver _webDriver;

        #endregion

        #region Enums

        private enum WebBrowsers
        {
            Firefox,
            Chrome,
            InternetExplorer
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MyWebDriver" /> class.
        /// </summary>
        /// <param name="browserType">Type of the browser.</param>
        public MyWebDriver(string browserType)
        {
            LogManager.GetCurrentClassLogger().Info("Opening {0} web browser.", browserType);
            switch (ParseWebBrowserToEnum(browserType.ToUpper()))
            {
                case WebBrowsers.Firefox:
                    _webDriver = new FirefoxDriver();
                    break;
                case WebBrowsers.InternetExplorer:
                    var options = new InternetExplorerOptions
                                      { IntroduceInstabilityByIgnoringProtectedModeSettings = true };
                    _webDriver = new InternetExplorerDriver(Config.AppSettings.WebDriversFolder, options);
                    break;
                case WebBrowsers.Chrome:
                    _webDriver = new ChromeDriver(Config.AppSettings.WebDriversFolder);
                    break;
                default:
                    _webDriver = null;
                    LogManager.GetCurrentClassLogger().Error("Web driver was not initialized correctly.");
                    return;
            }

            _webDriver.Manage().Timeouts().SetPageLoadTimeout(PageLoadTimeout);
            _webDriver.Manage().Timeouts().ImplicitlyWait(ImplicitlyWaitTime);
            ElementSearchTimeout = FindElementTimeout;
        }

        #endregion

        #region Properties

        public WebDriverWait DriverWait
        {
            get
            {
                return new WebDriverWait(_webDriver, ElementSearchTimeout);
            }
        }

        /// <summary>
        /// Returns WebDriver.
        /// </summary>
        public IWebDriver WebDriver
        {
            get
            {
                return _webDriver;
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                _webDriver.Dispose();
        }

        /// <summary>
        /// Gets or sets the element search timeout.
        /// </summary>
        /// <value>
        /// The element search timeout.
        /// </value>
        public TimeSpan ElementSearchTimeout { get; set; }

        /// <summary>
        /// Gets the page source.
        /// </summary>
        /// <value>
        /// The page source.
        /// </value>
        public string PageSource
        {
            get
            {
                return _webDriver.PageSource;
            }
        }

        /// <summary>
        /// Gets the driver action.
        /// </summary>
        /// <value>
        /// The driver action.
        /// </value>
        public Actions DriverAction
        {
            get
            {
                return new Actions(_webDriver);
            }
        }

        public IAlert Alert
        {
            get
            {
                return _webDriver.SwitchTo().Alert();
            }
        }

        /// <summary>
        /// Gets the current page title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            get
            {
                return _webDriver.Title;
            }
        }

        /// <summary>
        /// Gets the current page URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public Uri Url
        {
            get
            {
                return new Uri(_webDriver.Url);
            }
        }

        #endregion

        #region Non-Public Methods

        private static WebBrowsers ParseWebBrowserToEnum(string browser)
        {
            switch (browser)
            {
                case FIREFOX:
                    return WebBrowsers.Firefox;
                case CHROME:
                    return WebBrowsers.Chrome;
                case INTERNETEXPLORER:
                    return WebBrowsers.InternetExplorer;
                default:
                    return WebBrowsers.Firefox;
            }
        }

        /// <summary>
        /// Finds the element.
        /// </summary>
        /// <param name="searchCondition">The search condition.</param>
        /// <param name="wait">The wait.</param>
        /// <returns>Element that satisfy search condition.</returns>
        /// <exception cref="ElementNotFoundException">Indicate that no element was found.</exception>
        private static IWebElement FindElement(By searchCondition, DefaultWait<IWebDriver> wait)
        {
            try
            {
                return wait.Until(wDriver => wDriver.FindElement(searchCondition));
            }
            catch (NoSuchElementException exception)
            {
                LogManager.GetCurrentClassLogger().Warn("Element does not exist: '{0}'", searchCondition);
                throw new ElementNotFoundException(string.Format("Waiting for elements ends with timeout time {0}.", wait.Timeout), exception);
            }
            catch (WebDriverTimeoutException exception)
            {
                LogManager.GetCurrentClassLogger().Warn("Timeout after searching for element: '{0}'", searchCondition);
                throw new ElementNotFoundException(string.Format("Waiting for elements ends with timeout time {0}.", wait.Timeout), exception);
            }
        }

        /// <summary>
        /// Finds the elements.
        /// </summary>
        /// <param name="searchCondition">The search condition.</param>
        /// <param name="wait">The wait.</param>
        /// <returns>Collection of elements that satisfy the search condition.</returns>
        /// <exception cref="ElementNotFoundException">Indicate that no element was found.</exception>
        private static ReadOnlyCollection<IWebElement> FindElements(By searchCondition, DefaultWait<IWebDriver> wait)
        {
            try
            {
                return wait.Until(wDriver => wDriver.FindElements(searchCondition));
            }
            catch (WebDriverTimeoutException exception)
            {
                throw new ElementNotFoundException(string.Format("Waiting for elements ends with timeout time {0}.", wait.Timeout), exception);
            }
        }

        private static IWebElement FindElementVisible(By searchCondition, DefaultWait<IWebDriver> wait)
        {
            try
            {
                return wait.Until(ExpectedConditions.ElementIsVisible(searchCondition));
            }
            catch (WebDriverTimeoutException exception)
            {
                throw new ElementNotFoundException(string.Format("Waiting for elements ends with timeout time {0}.", wait.Timeout), exception);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Closes this instance of web driver.
        /// </summary>
        public void Close()
        {
            LogManager.GetCurrentClassLogger().Info("Closing web driver.");
            _webDriver.Close();
        }

        /// <summary>
        /// Instructs the driver to change its settings.
        /// </summary>
        /// <returns>Object representing options to change.</returns>
        public IOptions Manage()
        {
            return _webDriver.Manage();
        }

        /// <summary>
        /// Gets the web browser navigation object.
        /// </summary>
        /// <returns>Web browser navigation object.</returns>
        public INavigation Navigate()
        {
            return _webDriver.Navigate();
        }

        /// <summary>
        /// Quits this instance of web driver.
        /// </summary>
        public void Quit()
        {
            LogManager.GetCurrentClassLogger().Info("Quitting web driver.");
            _webDriver.Quit();
        }

        /// <summary>
        /// Searches web element for defined time.
        /// </summary>
        /// <param name="searchCondition">Condition that define element to search for it.</param>
        /// <param name="timeout">Time after which function will stop search and timeout exception will be thrown.</param>
        /// <returns>Web element object.</returns>
        public IWebElement FindElement(By searchCondition, int timeout)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for element: '{0}'", searchCondition);
            return FindElement(searchCondition, new WebDriverWait(_webDriver, TimeSpan.FromMilliseconds(timeout)));
        }

        /// <summary>
        /// Searches web element for defined time.
        /// </summary>
        /// <param name="searchCondition">Condition that define element to search for it.</param>
        /// <returns>Web element object.</returns>
        public IWebElement FindElement(By searchCondition)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for element: '{0}'", searchCondition);
            return FindElement(searchCondition, DriverWait);
        }

        public int GetCountOfElements(string xpathToCount)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for element under xpath: '{0}'", xpathToCount);
            return WebBrowser.WebDriver.FindElements(By.XPath(xpathToCount)).Count;
        } 

        /// <summary>
        /// Searches web element for defined time.
        /// </summary>
        /// <param name="elementXPath">Xpath that will identify searched element.</param>
        /// <param name="timeout">Time after which function will stop search and timeout exception will be thrown.</param>
        /// <returns>Web element object.</returns>
        public IWebElement FindElement(string elementXPath, int timeout)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for element: '{0}'", elementXPath);
            return FindElement(By.XPath(elementXPath), new WebDriverWait(_webDriver, TimeSpan.FromMilliseconds(timeout)));
        }

        /// <summary>
        /// Searches web element for defined time.
        /// </summary>
        /// <param name="elementXPath">Xpath that will identify searched element.</param>
        /// <returns>Web element object.</returns>
        public IWebElement FindElement(string elementXPath)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for element: '{0}'", elementXPath);
            return FindElement(By.XPath(elementXPath), DriverWait);
        }

        /// <summary>
        /// Searches web element list for defined time.
        /// </summary>
        /// <param name="searchCondition">Condition that define element to search for it.</param>
        /// <param name="timeout">Time after which function will stop search and timeout exception will be thrown.</param>
        /// <returns>Web elements objects collection.</returns>
        public ReadOnlyCollection<IWebElement> FindElements(By searchCondition, int timeout)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for elements: '{0}'", searchCondition);
            return FindElements(searchCondition, new WebDriverWait(_webDriver, TimeSpan.FromMilliseconds(timeout)));
        }

        /// <summary>
        /// Searches web element list for defined time.
        /// </summary>
        /// <param name="searchCondition">Condition that define element to search for it.</param>
        /// <returns>Web elements objects collection.</returns>
        public ReadOnlyCollection<IWebElement> FindElements(By searchCondition)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for elements: '{0}'", searchCondition);
            return FindElements(searchCondition, DriverWait);
        }

        /// <summary>
        /// Searches web element list for defined time.
        /// </summary>
        /// <param name="elementsXPath">Xpath that will identify searched elements.</param>
        /// <param name="timeout">Time after which function will stop search and timeout exception will be thrown.</param>
        /// <returns>Web elements objects collection.</returns>
        public IWebElement WaitForElement(string elementXPath, int timeout)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for elements: '{0}'", elementXPath);
            return FindElementVisible(By.XPath(elementXPath), new WebDriverWait(_webDriver, TimeSpan.FromMilliseconds(timeout)));
        }

        /// <summary>
        /// Searches web element list for defined time.
        /// </summary>
        /// <param name="elementsXPath">Xpath that will identify searched elements.</param>
        /// <returns>Web elements objects collection.</returns>
        public IWebElement WaitForElement(string elementXPath)
        {
            LogManager.GetCurrentClassLogger().Info("Waiting for elements: '{0}'", elementXPath);
            return FindElementVisible(By.XPath(elementXPath), DriverWait);
        }

        /// <summary>
        /// Extension method to execute java script code on web site driver.
        /// </summary>
        /// <param name="script">Script code to execute on current web driver.</param>
        /// <returns>Web Driver object after script execution.</returns>
        public object ExecuteScript(string script)
        {
            LogManager.GetCurrentClassLogger().Info("Executing on current page java script : \"{0}\"", script);

            var javaScriptExecutor = _webDriver as IJavaScriptExecutor;
            if (javaScriptExecutor == null) 
                return null;

            var result = javaScriptExecutor.ExecuteScript(script);
            LogManager.GetCurrentClassLogger().Info("Executed script returned: '{0}'", result);

            return result;
        }

        /// <summary>
        /// Navigates to the URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        public void NavigateToUrl(Uri url)
        {
            try
            {
                _webDriver.Navigate().GoToUrl(url);
            }
            catch (WebDriverTimeoutException)
            {
                LogManager.GetCurrentClassLogger().Info("Timeout during opening page '{0}'.", url);
            }
        }

        /// <summary>
        /// Checks if element defined by xpath exists.
        /// </summary>
        /// <param name="xpath">XPath to element you want to check.</param>
        /// <returns>True if element exists, False if does not.</returns>
        public bool CheckIfElementExists(string xpath)
        {
            ReadOnlyCollection<IWebElement> tested = null;
            try
            {
                tested = _webDriver.FindElements(By.XPath(xpath));
            }
            catch (NoSuchElementException exception)
            {
                LogManager.GetCurrentClassLogger().Warn(exception);
            }

            return tested != null;
        }

        public bool IsAlertPresent()
        {
            try
            {
                _webDriver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException ex)
            {
                LogManager.GetCurrentClassLogger().Warn(ex);
                return false;
            }
        }

        public void WaitUntilJQueryIsComplete()
        {
            var jQueryIsExecuted = false;
            do
            {
                jQueryIsExecuted = (bool)WebBrowser.WebDriver.ExecuteScript("return jQuery.active == 0");
                System.Threading.Thread.Sleep(1000);
            }
            while (!jQueryIsExecuted);
        }

        public void SwitchToFrame(string frameName)
        {
            _webDriver.SwitchTo().Frame(frameName);
        }

        public void SwitchToFrame(int frameIndex)
        {
            _webDriver.SwitchTo().Frame(frameIndex);
        }

        public void SwitchToTab(int tabID)
        {
            var tabs = new List<String>(_webDriver.WindowHandles);
            _webDriver.SwitchTo().Window(tabs[tabID]);
        }

        public void SwitchToParentFrame()
        {
            _webDriver.SwitchTo().ParentFrame();
        }
                
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
