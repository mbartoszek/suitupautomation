﻿using NLog;
using System;
using System.Threading;
using Automation.Configuration;

namespace Automation.Core
{
    /// <summary>
    /// Function to help with basic operations.
    /// </summary>
    public static class BasicHelper
    {
        #region Constants

        private const int SleepTimeout = 100;

        #endregion

        #region Methods

        /// <summary>
        /// Waits specific time for returning by action 'True'. 
        /// </summary>
        /// <param name="action">Method that indicates that condition is meet.</param>
        /// <param name="timeout">Time to wait for meet the condition.</param>
        /// <returns>Result of comparison. True if action return True in specified time.</returns>
        public static bool WaitForSuccess(Func<bool> action, int? timeout = null)
        {
            var timeoutValue = timeout ?? Config.AppSettings.DefaultFindElementTimeout;

            LogManager.GetCurrentClassLogger().Info("Waiting for condition {0}.", action.Method.Name);
            var start = DateTime.Now;
            double timeRun;

            do
            {
                Thread.Sleep(SleepTimeout);
                timeRun = (DateTime.Now - start).TotalMilliseconds;

                if (!action()) continue;

                LogManager.GetCurrentClassLogger().Info("Waiting ends sucessfully after {0} ms.", timeRun.ToString());
                return true;
            }
            while (timeRun <= timeoutValue);

            LogManager.GetCurrentClassLogger().Info("Waiting ends unsucessfully after timeout time: {0} ms.", timeout.ToString());
            return false;
        }

        /// <summary>
        /// Gets the URL.
        /// </summary>
        /// <param name="path">The path of the site.</param>
        /// <param name="hostName">The name of the host.</param>
        /// <param name="scheme">The scheme of the url.</param>
        /// <returns>The Uri.</returns>
        public static Uri GetUrl(string mainPath, string path, string port, string hostName = null, string scheme = null)
        {
            var fullPath = string.Format("{0}/{1}", mainPath, path);
            var parsedPort = 80;
            parsedPort = (port == string.Empty) || (int.TryParse(port, out parsedPort)) ? parsedPort : 80;

            return new UriBuilder
            { 
                Host = hostName, 
                Port = parsedPort,
                Path = fullPath, 
                Scheme = scheme ?? "http" 
            }.Uri;
        }

        #endregion
    }
}
