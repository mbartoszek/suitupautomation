﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;
using Automation.Configuration;

namespace Automation.Core
{
    [Serializable]
    [XmlRoot("Page")]
    public class PageDetails
    {
        #region Fields

        [XmlIgnore]
        private Dictionary<string, ControlDetail> _controlDetails;
        [XmlIgnore]
        private Dictionary<string, ControlHeaders> _controlHeaders;

        /// <summary>
        /// The type of details.
        /// </summary>
        [XmlIgnore]
        public string DetailsType = "Page";

        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        [XmlIgnore]
        public Dictionary<string, ControlDetail> ControlDetails
        {
            get {
                return _controlDetails ??
                       (_controlDetails =
                        ControlHelper.CreateControlDetails(ControlsDetailsList, ref DetailsType, null, Name));
            }
        }

        public Collection<ControlDetail> T;

        [XmlIgnore]
        public Dictionary<string, ControlHeaders> ControlHeaders
        {
            get
            {
                return _controlHeaders ??
                       (_controlHeaders =
                        ControlHelper.CreateControlHeaders(ControlsHeadersList));
            }
        }

        public Collection<ControlHeaders> H;

        /// <summary>
        /// Gets or sets the controls details list.
        /// </summary>
        /// <value>
        /// The controls details list.
        /// </value>
        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Collection is deserialized from xml so cannot be read only.")]
        [XmlArray("Controls")]
        [XmlArrayItem("Control", typeof(ControlDetail))]
        public Collection<ControlDetail> ControlsDetailsList
        {
            get { return T; }
            set { T = value; }
        }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Collection is deserialized from xml so cannot be read only.")]
        [XmlArray("Headers")]
        [XmlArrayItem("Header", typeof(ControlHeaders))]
        public Collection<ControlHeaders> ControlsHeadersList
        {
            get { return H; }
            set { H = value; }
        }

        /// <summary>
        /// Gets or sets the page name.
        /// </summary>
        /// <value>
        /// The page name.
        /// </value>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the page title.
        /// </summary>
        /// <value>
        /// The page title.
        /// </value>
        [XmlAttribute("title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets the page URL.
        /// </summary>
        /// <value>
        /// The page URL.
        /// </value>
        [XmlIgnore]
        public Uri Url { get; private set; }

        /// <summary>
        /// Gets or sets the page URL as text.
        /// </summary>
        /// <value>
        /// The page URL as text.
        /// </value>
        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "There must be deserialization from xml to string."), XmlAttribute("url")]
        public string UrlString
        {
            get
            {
                return Url.AbsoluteUri;
            }

            set
            {
                Config.Load();
                Url = BasicHelper.GetUrl(Config.AppSettings.MainPath, value, Config.AppSettings.Port, Config.AppSettings.HostName, Config.AppSettings.Scheme);
            }
        }

        #endregion
    }
}
