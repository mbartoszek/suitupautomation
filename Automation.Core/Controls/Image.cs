﻿namespace Automation.Core.Controls
{
    public class Image : Control
    {
        #region Constants

        protected const string ImageAttributeName = "value";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates label object with defined name.
        /// </summary>
        /// <param name="imageDetails">Name of label to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Image(ControlDetail imageDetails, int timeout)
            : base(imageDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the text from a image.
        /// </summary>
        public string Text
        {
            get
            {
                return WebElement.Text;
            }
        }

        /// <summary>
        /// Gets the value of the image.
        /// </summary>
        public string Value
        {
            get
            {
                return WebElement.GetAttribute(ImageAttributeName);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates label object with defined name.
        /// </summary>
        /// <param name="objectDetails">The label object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created label control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Image(objectDetails, timeout);
        }

        #endregion
    }
}
