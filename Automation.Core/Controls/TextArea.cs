﻿using Automation.Core.Interfaces;
using NLog;

namespace Automation.Core.Controls
{
    public class TextArea : Control, IEditable
    {
        #region Constants

        protected const string TextAttributeName = "value";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates text area object with defined name.
        /// </summary>
        /// <param name="textareaDetails">Name of text area to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public TextArea(ControlDetail textareaDetails, int timeout)
            : base(textareaDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value that is text from text area.
        /// </summary>
        public virtual string Text
        {
            get
            {
                return WebElement.GetAttribute(TextAttributeName);
            }

            set
            {
                WebElement.Clear();
                WebElement.Click();
                WebElement.SendKeys(value);
                BasicHelper.WaitForSuccess(() => WebElement.GetAttribute(TextAttributeName) == value);
                LogManager.GetCurrentClassLogger().Info("Entered {0} into textarea {1}.", value, ControlInformation.Name);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates text area object with defined name.
        /// </summary>
        /// <param name="objectDetails">The object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created text area control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new TextArea(objectDetails, timeout);
        }

        #endregion

        #region IEditable_Implementation

        /// <summary>
        /// Clears text area field.
        /// </summary>
        public virtual void Clear()
        {
            WebElement.Click();
            WebElement.Clear();
            LogManager.GetCurrentClassLogger().Info("textarea {0} cleared.", ControlInformation.Name);
        }

        #endregion
    }
}
