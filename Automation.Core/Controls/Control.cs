﻿using System;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automation.Core.Controls
{
    public abstract class Control
    {
        #region Constructors

        /// <summary>
        /// Creates base control object with defined name.
        /// </summary>
        /// <param name="controlDetails">Name of control to select.</param>
        protected Control(ControlDetail controlDetails)
        {
            Initialize(controlDetails);
        }

        /// <summary>
        /// Creates base control object with defined name.
        /// </summary>
        /// <param name="controlDetails">Name of control to select.</param>
        /// <param name="timeout">Default timeout for control.</param>
        protected Control(ControlDetail controlDetails, int timeout)
        {
            if (timeout > 0)
                Timeout = timeout;

            Initialize(controlDetails);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the web element.
        /// </summary>
        /// <value>
        /// The web element.
        /// </value>
        public IWebElement WebElement { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        public int Timeout { get; set; }

        /// <summary>
        /// Gets or sets the control information.
        /// </summary>
        /// <value>
        /// The control information.
        /// </value>
        public ControlDetail ControlInformation { get; set; }

        /// <summary>
        /// Gets the control name.
        /// </summary>
        /// <value>
        /// The control name.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        /// Check if element exist on page
        /// </summary>
        /// <returns>True if it's on page, false if not</returns>
        public bool IsDisplayed
        {
            get
            {
                return WebElement.Displayed;
            }
        }

        /// <summary>
        /// Get specified parameter value.
        /// </summary>
        /// <param name="parameterName">Parameter of your choice</param>
        /// <returns>Parameter value</returns>
        public string GetParameterValue(string parameterName)
        {
            return WebElement.GetAttribute(parameterName);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Initializes the specified control.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        private void Initialize(ControlDetail controlDetails)
        {
            ControlInformation = controlDetails;
            DefaultWait<IWebDriver> wait = Timeout > 0 ? new WebDriverWait(WebBrowser.WebDriver.WebDriver, TimeSpan.FromMilliseconds(Timeout)) : WebBrowser.WebDriver.DriverWait;
            
            try
            {
                WebElement = wait.Until(wDriver => wDriver.FindElement(By.XPath(controlDetails.Xpath)));
            }
            catch (NoSuchElementException exception)
            {
                LogManager.GetCurrentClassLogger().Warn("Element does not exist: '{0}'", exception);
                WebElement = null;
            }
            catch (WebDriverTimeoutException exception)
            {
                LogManager.GetCurrentClassLogger().Warn("Timeout after searching for element: '{0}'", exception);
                WebElement = null;
            }
            
            LogManager.GetCurrentClassLogger()
                .Info(WebElement != null ? "Control {0} found." : "Control {0} NOT found.", controlDetails.Name);
        }
        #endregion
    }
}
