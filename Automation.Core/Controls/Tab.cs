﻿using Automation.Core.Exceptions;
using Automation.Core.Interfaces;
using NLog;
using System;

namespace Automation.Core.Controls
{
    public class Tab : Control, IClickable
    {
        #region Constants

        private const string SelectedAttribute = "aria-selected";

        #endregion
        
        #region Constructors

        /// <summary>
        /// Creates tab object with defined name.
        /// </summary>
        /// <param name="buttonDetails">Name of tab to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Tab(ControlDetail buttonDetails, int timeout)
            : base(buttonDetails, timeout)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates tab object with defined name.
        /// </summary>
        /// <param name="objectDetails">The tab object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created tab control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Tab(objectDetails, timeout);
        }

        /// <summary>
        /// Determines whether Tab is opened.
        /// </summary>
        /// <returns>
        /// <c>true</c> if Tab is opened; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="IncorrectXmlDataException">Xpath to element is incorrect.</exception>
        public bool IsOpen()
        {
            var selected = WebElement.GetAttribute(SelectedAttribute);
            if (selected == null)
                throw new IncorrectXmlDataException(string.Format("Xpath to Tab element '{0}' is incorrect (attribute '{1}' does not exist) or there is only one one tab.", ControlInformation.Name, SelectedAttribute));

            return Convert.ToBoolean(selected);
        }

        #endregion

        #region IClickable_Implementation

        /// <summary>
        /// Clicks on control.
        /// </summary>
        public void Click()
        {
            WebElement.Click();
            LogManager.GetCurrentClassLogger().Info("Tab {0} clicked.", ControlInformation.Name);
        }      
        
        #endregion
    }
}
