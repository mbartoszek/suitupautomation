﻿namespace Automation.Core.Controls
{
    public class RadioButton : Button
    {
        #region Constructors

        /// <summary>
        /// Creates radio button object with defined name.
        /// </summary>
        /// <param name="radioButtonDetails">Name of button to select.</param>
        /// <param name="timeout">The control load timeout.</param>
        public RadioButton(ControlDetail radioButtonDetails, int timeout)
            : base(radioButtonDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether checkbox is checked.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates radio button object with defined name.
        /// </summary>
        /// <param name="objectDetails">The radio button object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created radio button control.
        /// </returns>
        public static new Control Create(ControlDetail objectDetails, int timeout)
        {
            return new RadioButton(objectDetails, timeout);
        }

        #endregion
    }
}