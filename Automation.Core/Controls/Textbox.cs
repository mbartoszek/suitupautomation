﻿using Automation.Core.Interfaces;
using NLog;

namespace Automation.Core.Controls
{
    public class Textbox : Control, IEditable
    {
        #region Constants

        protected const string TextAttributeName = "value";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates textbox object with defined name.
        /// </summary>
        /// <param name="textboxDetails">Name of textbox to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Textbox(ControlDetail textboxDetails, int timeout)
            : base(textboxDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value that is text from textbox.
        /// </summary>
        public virtual string Text
        {
            get
            {
                return WebElement.GetAttribute(TextAttributeName);
            }

            set
            {
                WebElement.Clear();
                WebElement.Click();
                
                WebElement.SendKeys(value);
                BasicHelper.WaitForSuccess(() => WebElement.GetAttribute(TextAttributeName) == value);
                LogManager.GetCurrentClassLogger().Info("Entered {0} into textbox {1}.", value, ControlInformation.Name);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates textbox object with defined name.
        /// </summary>
        /// <param name="objectDetails">The object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created textbox control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Textbox(objectDetails, timeout);
        }

        #endregion

        #region IEditable_Implementation

        /// <summary>
        /// Clears textbox field.
        /// </summary>
        public virtual void Clear()
        {
            WebElement.Click();
            WebElement.Clear();
            LogManager.GetCurrentClassLogger().Info("Textbox {0} cleared.", ControlInformation.Name);
        }

        #endregion

        /// <summary>
        /// Sends keys textbox field.
        /// </summary>
        public virtual void SendKeys(string text)
        {
            WebElement.SendKeys(text);
            LogManager.GetCurrentClassLogger().Info("Textbox {0} sent.", ControlInformation.Name);
        }

        public virtual void SetFocus()
        {
            WebElement.Click();
        }
    }
}
