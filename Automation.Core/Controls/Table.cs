﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Automation.Core.Controls
{
    public class Table : Control
    {
        private readonly string _tableXpath;
        private List<TableColumn> _tableColumnList;

        #region Constructors

        public Table(ControlDetail tableDetails, int timeout, Dictionary<string, ControlHeaders> controlHeaders)
            : base(tableDetails, timeout)
        {
            var filteredHeaders = controlHeaders.Where(header => header.Value.TableName == tableDetails.Name).ToDictionary(header => header.Key, header => header.Value);

            _tableXpath = tableDetails.Xpath;
            InitializeTableColumns(filteredHeaders);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value that is text from table.
        /// </summary>
        public virtual string Text
        {
            get
            {
                return WebElement.Text;
            }
        }

        public List<TableColumn> Columns
        {
            get
            {
                return _tableColumnList;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates table object with defined name.
        /// </summary>
        /// <param name="objectDetails">The table object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created table control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout, Dictionary<string, ControlHeaders> controlHeaders)
        {
            return new Table(objectDetails, timeout, controlHeaders);
        }

        public bool CheckIfItemExistsInTable(string item)
        {
            return WebElement.Text.Contains(item);
        }

        public string ReadCellValue(int columnIndex, int rowIndex)
        {
            var cellXpath = string.Format("{0}/tbody/tr[{1}]/td[{2}]", _tableXpath, ++rowIndex, columnIndex);

            try
            {
                var cellControl = WebBrowser.WebDriver.FindElement(cellXpath);
                return cellControl.Text;
            }
            catch (Exception)
            {
                throw new NoSuchElementException("Table does not contain cell with provided parameters, invalid indexes");
            }   
        }

        public string ReadCellValue(string columnName, int rowIndex)
        {
            var cellXpath = string.Format("{0}/tbody/tr[{1}]/td[@aria-describedby='{2}']", _tableXpath, ++rowIndex, columnName);

            try
            {
                var cellControl = WebBrowser.WebDriver.FindElement(cellXpath);
                return cellControl.Text;
            }
            catch (Exception)
            {
                throw new NoSuchElementException("Table does not contain cell with provided parameters, invalid indexes");
            }
        }

        public string ReadCellValue(int rowIndex)
        {
            var cellXpath = string.Format("{0}/tbody/tr[{1}]/td[2]", _tableXpath, rowIndex);

            try
            {
                var cellControl = WebBrowser.WebDriver.FindElement(cellXpath);
                return cellControl.Text;
            }
            catch (Exception)
            {
                throw new NoSuchElementException("Table does not contain cell with provided parameters, invalid indexes");
            }
        }

        public int RowsCount()
        {
            return WebBrowser.WebDriver.GetCountOfElements(string.Format("{0}/tbody/tr", _tableXpath)) - 1;
        }

        public void InitializeTableColumns(Dictionary<string, ControlHeaders> columnHeaders)
        {
            var columnIndex = 0;
            _tableColumnList = new List<TableColumn>();

            foreach (var header in columnHeaders)
            {
                columnIndex++;
                _tableColumnList.Add(new TableColumn(header.Key, columnIndex, header.Value.Header));
            }
        }

        #endregion
    }

    public class TableColumn
    {
        private readonly string _columnName;
        private readonly int _columnIndex;
        private readonly string _columnAriaDescribedBy;

        public string ColumnName
        {
            get { return _columnName; }
        }

        public int ColumnIndex
        {
            get { return _columnIndex; }
        }

        public string ColumnAriaDescribedBy
        {
            get { return _columnAriaDescribedBy; }
        }

        public TableColumn(string name, int index, string aria)
        {
            _columnIndex = index;
            _columnName = name;
            _columnAriaDescribedBy = aria;
        }
    }
}
