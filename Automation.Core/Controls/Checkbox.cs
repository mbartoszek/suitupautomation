﻿using Automation.Core.Interfaces;
using NLog;

namespace Automation.Core.Controls
{
    public class Checkbox : Control, ISelectable
    {
        #region Constructors

        /// <summary>
        /// Creates checkbox object with defined name.
        /// </summary>
        /// <param name="checkboxDetails">Name of checkbox to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Checkbox(ControlDetail checkboxDetails, int timeout)
            : base(checkboxDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether checkbox is checked.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return WebElement.Selected;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates checkbox object with defined name.
        /// </summary>
        /// <param name="objectDetails">The checkbox object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created checkbox control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Checkbox(objectDetails, timeout);
        }

        #endregion

        #region ISelectable_Implementation

        /// <summary>
        /// Checks checkbox.
        /// </summary>
        public void Select()
        {
            if (!IsSelected)
            {
                WebElement.Click();
                LogManager.GetCurrentClassLogger().Info("Checkbox {0} checked", ControlInformation.Name);
                return;
            }

            LogManager.GetCurrentClassLogger().Warn("Checkbox {0} already checked", ControlInformation.Name);
        }

        public void ClickHiddenCheckbox()
        {
            WebElement.Click();
        }

        /// <summary>
        /// Unchecks checkbox.
        /// </summary>
        public void Deselect()
        {
            if (IsSelected)
            {
                WebElement.Click();
                LogManager.GetCurrentClassLogger().Info("Checkbox {0} unchecked", ControlInformation.Name);
                return;
            }

            LogManager.GetCurrentClassLogger().Warn("Checkbox {0} already unchecked", ControlInformation.Name);
        }       

        #endregion
    }
}
