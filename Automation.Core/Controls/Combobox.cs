﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Automation.Core.Interfaces;
using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automation.Core.Controls
{
    public class Combobox : Control, ISingleSelectableItems
    {
        #region Fields

        private readonly SelectElement _select;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates combo box object with defined name.
        /// </summary>
        /// <param name="comboboxDetails">Name of combo box to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Combobox(ControlDetail comboboxDetails, int timeout)
            : base(comboboxDetails, timeout)
        {
            _select = new SelectElement(WebElement);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the options.
        /// </summary>
        /// <value>
        /// The options.
        /// </value>
        public List<string> Options
        {
            get
            {
                return _select.Options.Select(option => option.Text).ToList();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates combo box object with defined name.
        /// </summary>
        /// <param name="objectDetails">The combo box object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created combo box control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Combobox(objectDetails, timeout);
        }

        #endregion

        #region ISingleSelectableItems_Implementation

        /// <summary>
        /// Gets the text from selected item.
        /// </summary>
        /// <returns>Text from the selected item.</returns>
        public string GetSelectedItemText()
        {
            if (_select.IsMultiple)
            {
                LogManager.GetCurrentClassLogger().Error("Combobox does not allow multiple selection. Element is wrong so selected text is not proper.");
                return null;
            }

            try
            {
                return _select.SelectedOption.Text;
            }
            catch (NoSuchElementException)
            {
                LogManager.GetCurrentClassLogger().Warn("There is no selected element in combobox");
                return string.Empty;
            }
        }

        /// <summary>
        /// Selects option by text.
        /// </summary>
        /// <param name="text">Text of option to be selected.</param>
        public void SelectItem(string text)
        {
            _select.SelectByText(text);
        }

        /// <summary>
        /// Selects option by index.
        /// </summary>
        /// <param name="index">Index of option to be selected.</param>
        public void SelectItem(int index)
        {
            WebElement.Click();
            for (var i = 0; i < index; i++)
                WebElement.SendKeys(Keys.Down);
            Thread.Sleep(50);
            WebElement.SendKeys(Keys.Enter);
            LogManager.GetCurrentClassLogger().Info("Selecting combobox option: '{0}' by index: {1}.", _select.Options[index].Text, index);
        }

        public void SelectItemByValue(string selectedValue)
        {
            _select.SelectByValue(selectedValue);
        }

        /// <summary>
        /// Deselecting option by text.
        /// </summary>
        /// <param name="text">Text in option to be selected.</param>
        public void DeselectItem(string text)
        {
            _select.DeselectByText(text);
            LogManager.GetCurrentClassLogger().Info("Deselecting {0} combobox option by text.", text);
        }

        /// <summary>
        /// Deselecting option by index.
        /// </summary>
        /// <param name="index">Index of option to be deselected.</param>
        public void DeselectItem(int index)
        {
            _select.DeselectByIndex(index);
            LogManager.GetCurrentClassLogger().Info("Deselecting combobox option: '{0}' by index: {1}.", _select.Options[index].Text, index);
        }

        #endregion
    }
}
