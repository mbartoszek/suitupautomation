﻿using Automation.Core.Interfaces;
using NLog;
using System;

namespace Automation.Core.Controls
{
    public class Link : Control, IClickable, ILinkable
    {
        #region Fields

        private const string UrlAttributeName = "href";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates link object with defined name.
        /// </summary>
        /// <param name="linkDetails">Name of link to select.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Link(ControlDetail linkDetails, int timeout)
            : base(linkDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the URL of hyperlink.
        /// </summary>
        public Uri Url
        {
            get
            {
                return new Uri(WebElement.GetAttribute(UrlAttributeName));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates link object with defined name.
        /// </summary>
        /// <param name="objectDetails">The link object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created link control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Link(objectDetails, timeout);
        }

        #endregion

        #region IClickable_Implementation

        /// <summary>
        /// Clicks selected hyperlink.
        /// </summary>
        public void Click()
        {
            WebElement.Click();
            LogManager.GetCurrentClassLogger().Info("Clicked link: '{0}'.", ControlInformation.Name);
        }

        #endregion
    }
}
