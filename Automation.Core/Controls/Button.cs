﻿using Automation.Core.Interfaces;
using NLog;

namespace Automation.Core.Controls
{
    public class Button : Control, IClickable
    {
        #region Constructors

        /// <summary>
        /// Creates button object with defined name.
        /// </summary>
        /// <param name="buttonDetails">Name of button to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Button(ControlDetail buttonDetails, int timeout)
            : base(buttonDetails, timeout)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates button object with defined name.
        /// </summary>
        /// <param name="objectDetails">The button object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created button control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Button(objectDetails, timeout);
        }

        /// <summary>
        /// Determines whether this button is disabled.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this button is disabled; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDisabled()
        {
            return !WebElement.Enabled;
        }

        #endregion

        #region IClickable_Implementation

        /// <summary>
        /// Clicks selected button.
        /// </summary>
        public void Click()
        {
            WebElement.Click();
            LogManager.GetCurrentClassLogger().Info("Button {0} clicked.", ControlInformation.Name);
        }

        #endregion
    }
}
