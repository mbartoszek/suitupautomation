﻿using Automation.Core.Interfaces;
using NLog;

namespace Automation.Core.Controls
{
    public class Span : Control, IClickable
    {
        #region Constants

        protected const string SpanAttributeName = "value";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates Span object with defined name.
        /// </summary>
        /// <param name="spanDetails">Name of Span to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Span(ControlDetail spanDetails, int timeout)
            : base(spanDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the text from a span.
        /// </summary>
        public string Text
        {
            get
            {
                return WebElement.Text;
            }
        }

        /// <summary>
        /// Gets the value of the span.
        /// </summary>
        public string Value
        {
            get
            {
                return WebElement.GetAttribute(SpanAttributeName);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates Span object with defined name.
        /// </summary>
        /// <param name="objectDetails">The Span object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created Span control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Span(objectDetails, timeout);
        }

        /// <summary>
        /// Determines whether this Span is disabled.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this Span is disabled; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDisabled()
        {
            var className = WebElement.GetAttribute("class");
            if (className == null)
            {
                LogManager.GetCurrentClassLogger().Warn("Span '{0}' does not contain class attribute. Verify that xpath to element is correct.");
                return false;
            }

            return className.Contains("disabled");
        }

        #endregion

        #region IClickable_Implementation

        /// <summary>
        /// Clicks selected Span.
        /// </summary>
        public void Click()
        {
            WebElement.Click();
            LogManager.GetCurrentClassLogger().Info("Span {0} clicked.", ControlInformation.Name);
        }

        #endregion
    }
}
