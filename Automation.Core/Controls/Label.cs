﻿namespace Automation.Core.Controls
{
    public class Label : Control
    {
        #region Constants

        protected const string LabelAttributeName = "value";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates label object with defined name.
        /// </summary>
        /// <param name="labelDetails">Name of label to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Label(ControlDetail labelDetails, int timeout)
            : base(labelDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the text from a label.
        /// </summary>
        public string Text
        {
            get
            {
                return WebElement.Text;
            }
        }

        /// <summary>
        /// Gets the value of the label.
        /// </summary>
        public string Value
        {
            get
            {
                return WebElement.GetAttribute(LabelAttributeName);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates label object with defined name.
        /// </summary>
        /// <param name="objectDetails">The label object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created label control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Label(objectDetails, timeout);
        }

        #endregion
    }
}
