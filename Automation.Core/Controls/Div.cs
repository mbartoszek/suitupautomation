﻿using Automation.Core.Interfaces;
using NLog;

namespace Automation.Core.Controls
{
    public class Div : Control, IClickable
    {
        #region Constants

        protected const string DivAttributeName = "value";
        protected const string StyleAttributeValue = "style";

        #endregion

        #region Constructors

        /// <summary>
        /// Creates section object with defined name.
        /// </summary>
        /// <param name="divDetails">Name of section to create.</param>
        /// <param name="timeout">The control load timeout.</param>
        public Div(ControlDetail divDetails, int timeout)
            : base(divDetails, timeout)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the text from a section.
        /// </summary>
        public string Text
        {
            get
            {
                return WebElement.Text;
            }
        }

        /// <summary>
        /// Gets the value of the section.
        /// </summary>
        public string Value
        {
            get
            {
                return WebElement.GetAttribute(DivAttributeName);
            }
        }

        /// <summary>
        /// Gets the style attribute value of the section.
        /// </summary>
        public string Style
        {
            get
            {
                return WebElement.GetAttribute(StyleAttributeValue);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates section object with defined name.
        /// </summary>
        /// <param name="objectDetails">The section object details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created section control.
        /// </returns>
        public static Control Create(ControlDetail objectDetails, int timeout)
        {
            return new Div(objectDetails, timeout);
        }

        /// <summary>
        /// Determines whether this section is disabled.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this section is disabled; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDisabled()
        {
            var className = WebElement.GetAttribute("class");
            if (className == null)
            {
                LogManager.GetCurrentClassLogger().Warn("Div '{0}' does not contain class attribute. Verify that xpath to element is correct.");
                return false;
            }

            return className.Contains("disabled");
        }

        #endregion

        #region IClickable_Implementation

        /// <summary>
        /// Clicks selected section.
        /// </summary>
        public void Click()
        {
            WebElement.Click();
            LogManager.GetCurrentClassLogger().Info("Div {0} clicked.", ControlInformation.Name);
        }

        #endregion
    }
}
