﻿using System.Collections.Generic;
using System.Linq;

namespace Automation.Core
{
    public abstract class ControlContainer
    {
        #region Properties

        /// <summary>
        /// Gets or sets the default timeout.
        /// </summary>
        /// <value>
        /// The default timeout.
        /// </value>
        protected int DefaultTimeout { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the control timeout.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <returns>Correct control timeout.</returns>
        public int GetControlTimeout(int timeout)
        {
            return timeout == 0 ? DefaultTimeout : timeout;
        }

        /// <summary>
        /// Gets the controls of given type.
        /// </summary>
        /// <typeparam name="T">Type of controls to return.</typeparam>
        /// <returns>List of controls.</returns>
        public IList<T> GetControls<T>() where T : class
        {
            var props = GetType().GetProperties().Where(control => control.PropertyType == typeof(T));
            return props.Select(propertyInfo => propertyInfo.GetValue(this, null) as T).ToList();
        }

        #endregion
    }
}
