﻿using Automation.Configuration;

namespace Automation.Core
{
    /// <summary>
    /// Class with functions to help with basic operations.
    /// </summary>
    public static class TimeoutHelper
    {
        #region Constants

        private const string TimeoutAttributeName = "timeout";

        #endregion

        #region Methods

        /// <summary>
        /// Gets the element`s timeout.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlFileSchema">The XML file schema.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="elementType">Type of the element.</param>
        /// <param name="elementParentType">Type of element`s parent.</param>
        /// <returns>
        /// Timeout for control.
        /// </returns>
        public static int GetElementTimeout(string xmlFilePath, string xmlFileSchema, string elementName, string elementType, string elementParentType)
        {         
            var tempTimeout = XmlHelper.GetElementByName(xmlFilePath, xmlFileSchema, elementType, elementName).GetAttribute(TimeoutAttributeName, string.Empty);
            int timeOut;
            if (int.TryParse(tempTimeout, out timeOut))
                return timeOut;

            var nodes = XmlHelper.GetNodeListByXpath(xmlFilePath, xmlFileSchema, string.Format(@"//{0}", elementParentType));
            nodes.MoveNext();
            tempTimeout = nodes.Current.GetAttribute(TimeoutAttributeName, string.Empty);
            Config.Load();

            return int.Parse(!string.IsNullOrEmpty(tempTimeout) ? tempTimeout : Config.AppSettings.DefaultFindElementTimeout.ToString());
        }

        /// <summary>
        /// Gets the page timeout.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlFileSchema">The XML file schema.</param>
        /// <param name="scopeElementName">Name of the scope element.</param>
        /// <returns>
        /// Timeout for element in milliseconds.
        /// </returns>
        public static int GetTimeout(string xmlFilePath, string xmlFileSchema, string scopeElementName)
        {
            return GetElementTimeout(xmlFilePath, xmlFileSchema, scopeElementName, "Page", "Pages");
        }

        #endregion
    }
}
