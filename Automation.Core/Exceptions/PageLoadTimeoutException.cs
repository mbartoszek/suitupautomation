﻿using System;
using System.Runtime.Serialization;
using NLog;

namespace Automation.Core.Exceptions
{
    [Serializable]
    public class PageLoadTimeoutException : Exception
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PageLoadTimeoutException" /> class.
        /// </summary>
        public PageLoadTimeoutException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageLoadTimeoutException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public PageLoadTimeoutException(string message)
            : base(message)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageLoadTimeoutException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public PageLoadTimeoutException(string message, Exception innerException)
            : base(message, innerException)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageLoadTimeoutException" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected PageLoadTimeoutException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
    }
}
