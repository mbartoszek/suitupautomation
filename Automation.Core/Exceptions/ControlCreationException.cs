﻿using NLog;
using System;
using System.Runtime.Serialization;

namespace Automation.Core.Exceptions
{
    [Serializable]
    public class ControlCreationException : Exception
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlCreationException" /> class.
        /// </summary>
        public ControlCreationException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlCreationException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public ControlCreationException(string message) 
            : base(message)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlCreationException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ControlCreationException(string message, Exception innerException) 
            : base(message, innerException)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlCreationException" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected ControlCreationException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }

        #endregion
    }
}
