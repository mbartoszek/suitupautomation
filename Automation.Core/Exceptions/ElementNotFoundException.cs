﻿using NLog;
using System;
using System.Runtime.Serialization;

namespace Automation.Core.Exceptions
{
    [Serializable]
    public class ElementNotFoundException : Exception
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementNotFoundException" /> class.
        /// </summary>
        public ElementNotFoundException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementNotFoundException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public ElementNotFoundException(string message) 
            : base(message)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementNotFoundException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ElementNotFoundException(string message, Exception innerException) 
            : base(message, innerException)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementNotFoundException" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected ElementNotFoundException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }

        #endregion
    }
}
