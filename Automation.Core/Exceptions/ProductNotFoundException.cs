﻿using NLog;
using System;
using System.Runtime.Serialization;

namespace Automation.Core.Exceptions
{
    [Serializable]
    public class ProductNotFoundException : Exception
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNotFoundException" /> class.
        /// </summary>
        public ProductNotFoundException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNotFoundException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public ProductNotFoundException(string message) 
            : base(message)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNotFoundException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ProductNotFoundException(string message, Exception innerException) 
            : base(message, innerException)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNotFoundException" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected ProductNotFoundException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }

        #endregion
    }
}
