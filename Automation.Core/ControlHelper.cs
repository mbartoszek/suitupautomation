﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Automation.Core.Controls;

namespace Automation.Core
{
    /// <summary>
    /// Class with functions to help with basic operations.
    /// </summary>
    public static class ControlHelper
    {
        #region Methods

        /// <summary>
        /// Creates the control details.
        /// </summary>
        /// <param name="controlDetailsList">The control details list.</param>
        /// <param name="controlType">Type of the control.</param>
        /// <param name="controlName">Name of the control.</param>
        /// <param name="pageName">Name of the page.</param>
        /// <returns>Dictionary with correct control details.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "1#", Justification = "I must modify this field. Is better that way than code duplication.")]
        public static Dictionary<string, ControlDetail> CreateControlDetails(Collection<ControlDetail> controlDetailsList, ref string controlType, string controlName, string pageName)
        {
            if (controlType != null && controlType != "Page" && controlType != "Frame" && controlDetailsList.Any())
                controlType = string.Format("{0}_{1}", controlType, controlName);
             
            //foreach (var control in controlDetailsList)
            //    control.PageName = pageName;

            var controlDetails = controlDetailsList.ToDictionary(controlDetail => controlDetail.Name);

            return controlDetails;
        }

        public static Dictionary<string, ControlHeaders> CreateControlHeaders(Collection<ControlHeaders> controlHeadersList)
        {
            var controlHeaders = controlHeadersList.ToDictionary(controlHeader => controlHeader.Name);

            return controlHeaders;
        }

        /// <summary>
        /// Checks if text exists in controls.
        /// </summary>
        /// <param name="element">Label controls.</param>
        /// <param name="text">Text to find.</param>
        /// <param name="searchChild">Determines whether child controls should be searched.</param>
        /// <returns>True if text is found, otherwise false.</returns>
        public static bool FindTextInControls(Control element, string text, bool searchChild)
        {
            var xpathLabel = element.ControlInformation.Xpath;
            if (!searchChild)
                return WebBrowser.WebDriver.FindElement(string.Format("({0} [contains(.,'{1}')])", xpathLabel, text)) != null;

           return WebBrowser.WebDriver.FindElement(string.Format("{0}/child::*[contains(.,'{1}')]", xpathLabel, text)) != null;
        }

        #endregion
    }
}
