﻿using System;

namespace Automation.Core.Interfaces
{
    public interface ILinkable
    {
        #region Properties

        /// <summary>
        /// Gets the value of url in control.
        /// </summary>
        Uri Url { get; }

        #endregion
    }
}
