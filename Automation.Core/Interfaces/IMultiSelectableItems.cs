﻿using System.Collections.ObjectModel;

namespace Automation.Core.Interfaces
{
    public interface IMultiSelectableItems
    {
        #region Methods

        /// <summary>
        /// Gets the text from selected items.
        /// </summary>
        /// <returns>List of text from selected items.</returns>
        Collection<string> GetSelectedItemsText();

        /// <summary>
        /// Selects all items with chosen text.
        /// </summary>
        /// <param name="valuesToSelect">List of texts representing items to selection.</param>
        void SelectItems(Collection<string> valuesToSelect);

        /// <summary>
        /// Selects all items with chosen indexes.
        /// </summary>
        /// <param name="indexesToDeselect">List of indexes representing items to select.</param>
        void SelectItems(Collection<int> indexesToDeselect);

        /// <summary>
        /// Selects all items in this control.
        /// </summary>
        void SelectAll();

        /// <summary>
        /// Deselects all items with chosen text.
        /// </summary>
        /// <param name="valuesToSelect">List of texts representing items to deselect.</param>
        void DeselectItems(Collection<string> valuesToSelect);

        /// <summary>
        /// Deselects all items with chosen indexes.
        /// </summary>
        /// <param name="indexesToDeselect">List of indexes representing items to deselect.</param>
        void DeselectItems(Collection<int> indexesToDeselect);

        /// <summary>
        /// Deselects all items in this control.
        /// </summary>
        void DeselectAll();

        #endregion
    }
}
