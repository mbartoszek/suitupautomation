﻿namespace Automation.Core.Interfaces
{
    public interface ISelectable
    {
        #region Properties

        /// <summary>
        /// Gets a value indicating whether control is selected.
        /// </summary>
        bool IsSelected { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Selects control.
        /// </summary>
        void Select();

        /// <summary>
        /// Deselects control.
        /// </summary>
        void Deselect();

        #endregion
    }
}
