﻿namespace Automation.Core.Interfaces
{
    public interface ISingleSelectableItems
    {
        #region Methods

        /// <summary>
        /// Gets the text of selected item in control.
        /// </summary>
        /// <returns>Text of selected item in control.</returns>
        string GetSelectedItemText();

        /// <summary>
        /// Selects item in control by text.
        /// </summary>
        /// <param name="text">Text which specifies item to select.</param>
        void SelectItem(string text);

        /// <summary>
        /// Selects item in control by index.
        /// </summary>
        /// <param name="index">Text which specifies item to select.</param>
        void SelectItem(int index);

        /// <summary>
        /// Deselects item in control by text.
        /// </summary>
        /// <param name="text">Text which specifies item to deselect.</param>
        void DeselectItem(string text);

        /// <summary>
        /// Deselects item in control by index.
        /// </summary>
        /// <param name="index">Index which specifies item to deselect.</param>
        void DeselectItem(int index);
        
        #endregion
    }
}
