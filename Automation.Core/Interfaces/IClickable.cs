﻿namespace Automation.Core.Interfaces
{
    public interface IClickable
    {
        #region Methods

        /// <summary>
        /// Clicks on control.
        /// </summary>
        void Click();

        #endregion
    }
}
