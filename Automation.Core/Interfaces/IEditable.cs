﻿namespace Automation.Core.Interfaces
{
    public interface IEditable
    {
        #region Properties

        /// <summary>
        /// Gets or sets the text in control.
        /// </summary>
        string Text { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clears the text in control.
        /// </summary>
        void Clear();

        #endregion
    }
}
