﻿using System;

namespace Automation.Core.Pages
{
    public partial class NavigationPage
    {
        private enum NavigationMenus
        {
            Home,
            OutfitBuilder,
            InspireMe,
            Profile
        }

        private NavigationMenus ParseNavigationMenusToEnum(string navigationMenu)
        {
            switch (navigationMenu)
            {
                case "Home":
                    return NavigationMenus.Home;
                case "Outfit Builder":
                    return NavigationMenus.OutfitBuilder;
                case "Inspire Me":
                    return NavigationMenus.InspireMe;
                case "Profile":
                    return NavigationMenus.Profile;
                default:
                    throw new ArgumentException("Invalid Navigation Menu item provided as parameter");
            }
        }

        public T ClickOnNavigationBurger<T>() where T : NavigationPage, new()
        {
            Controls.btnNavigation.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new T();
        }

        public T ChooseOptionFromNavigationMenu<T>(string navigationItem) where T : NavigationPage, new()
        {
            switch (ParseNavigationMenusToEnum(navigationItem))
            {
                case NavigationMenus.Home:
                    Controls.lnkHomeOption.Click();
                    break;
                case NavigationMenus.OutfitBuilder:
                    Controls.lnkOutfitBuilderOption.Click();
                    break;
                case NavigationMenus.InspireMe:
                    Controls.lnkInspireMeOption.Click();
                    break;
                case NavigationMenus.Profile:
                    Controls.lnkProfileOption.Click();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            WaitForLoad();

            return new T();
        }
    }
}
