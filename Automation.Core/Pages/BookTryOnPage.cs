﻿namespace Automation.Core.Pages
{
    public partial class BookTryOnPage
    {
        public OutfitBuilderPage ClickOnGetStartedTryOnButton()
        {
            Controls.btnGetStartedTryOn.Click();
            WaitForLoad();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnGetStartedOutfitBuilderButton()
        {
            Controls.btnGetStartedOutfitBuilder.Click();
            WaitForLoad();

            return new OutfitBuilderPage();
        }
    }
}
