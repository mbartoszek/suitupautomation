﻿using System;
using Automation.Configuration;
using OpenQA.Selenium;

namespace Automation.Core.Pages
{
    public partial class InspireMePage
    {
        public enum CarouselIndicators
        {
            TheFirstOutfit = 1,
            TheSecondOutfit,
            TheThirdOutfit,
            TheFourthOutfit,
            TheFifthOutfit,
            TheSixthOutfit
        }

        private static CarouselIndicators ParseCarouselIndicatorsToEnum(string indicator)
        {
            switch (indicator)
            {
                case Defaults.InspireMeTheFirstOutfit:
                    return CarouselIndicators.TheFirstOutfit;
                case Defaults.InspireMeTheSecondtOutfit:
                    return CarouselIndicators.TheSecondOutfit;
                case Defaults.InspireMeTheThirdOutfit:
                    return CarouselIndicators.TheThirdOutfit;
                case Defaults.InspireMeTheFourthOutfit:
                    return CarouselIndicators.TheFourthOutfit;
                case Defaults.InspireMeTheFifthOutfit:
                    return CarouselIndicators.TheFifthOutfit;
                case Defaults.InspireMeTheSixthOutfit:
                    return CarouselIndicators.TheSixthOutfit;
                default:
                    throw new ArgumentException("Carousel Indicator does not match with any existing one");
            }
        }

        public void ChangeViewOnCarousel(string indicator)
        {
            var carouselIndicatorsXpath = Controls.divCarouselInspireIndicators.ControlInformation.Xpath;

            switch (ParseCarouselIndicatorsToEnum(indicator))
            {
                case CarouselIndicators.TheFirstOutfit:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.TheFirstOutfit))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.TheSecondOutfit:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.TheSecondOutfit))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.TheThirdOutfit:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.TheThirdOutfit))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.TheFourthOutfit:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.TheFourthOutfit))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.TheFifthOutfit:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.TheFifthOutfit))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.TheSixthOutfit:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.TheSixthOutfit))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public OutfitBuilderPage ClickOnViewInOutfitBuilderButton(string number)
        {
            switch (number)
            {
                case Defaults.InspireMeTheFirstOutfit:
                    Controls.btnViewInOutfitBuilder1.Click();
                    break;
                case Defaults.InspireMeTheSecondtOutfit:
                    Controls.btnViewInOutfitBuilder2.Click();
                    break;
                case Defaults.InspireMeTheThirdOutfit:
                    Controls.btnViewInOutfitBuilder3.Click();
                    break;
                case Defaults.InspireMeTheFourthOutfit:
                    Controls.btnViewInOutfitBuilder4.Click();
                    break;
                case Defaults.InspireMeTheFifthOutfit:
                    Controls.btnViewInOutfitBuilder5.Click();
                    break;
                case Defaults.InspireMeTheSixthOutfit:
                    Controls.btnViewInOutfitBuilder6.Click();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            WaitForLoad();
            return new OutfitBuilderPage();
        }
    }
}
