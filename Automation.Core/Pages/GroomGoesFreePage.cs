﻿namespace Automation.Core.Pages
{
    public partial class GroomGoesFreePage
    {
        public OutfitBuilderPage ClickOnGetStartedButton()
        {
            Controls.btnGetStarted.Click();
            WaitForLoad();

            return new OutfitBuilderPage();
        }
    }
}
