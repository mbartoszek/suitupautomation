﻿using System;
using System.Collections.Generic;
using System.Linq;
using Automation.Configuration;
using Automation.Core.Controls;
using OpenQA.Selenium;

namespace Automation.Core.Pages
{
    public partial class OutfitBuilderPage
    {
        private readonly List<bool> _checkImage = new List<bool>();
        private readonly List<Div> _outfitCategoriesList = new List<Div>();

        #region Enums

        private enum OutfitCategories
        {
            Suits,
            Waistcoats,
            Neckwear,
            Shirts,
            Accessories
        }

        private enum Styles
        {
            Tailcoat = 1,
            ShortJacket,
            PrinceEdward
        }

        private enum Colours
        {
            Black = 2,
            Blue = 3,
            White = 4,
            Red = 6,
            Green = 7,
            Yellow = 8,
            Brown = 9,
            Orange = 10,
            Pink = 11,
            Purple = 12,
            Grey = 13,
            Teal = 15,
            Cream = 16
        }

        #endregion

        #region EnumMethods

        private static Styles ParseOutfitStylesToEnum(string style)
        {
            switch (style)
            {
                case "Tailcoat":
                    return Styles.Tailcoat;
                case "Short Jacket":
                    return Styles.ShortJacket;
                case "Prince Edward":
                    return Styles.PrinceEdward;
                default:
                    throw new ArgumentException("Invalid Outfit Style provided as parameter");
            }
        }

        private static Colours ParseColoursToEnum(string colour)
        {
            switch (colour)
            {
                case "Black":
                    return Colours.Black;
                case "Blue":
                    return Colours.Blue;
                case "White":
                    return Colours.White;
                case "Red":
                    return Colours.Red;
                case "Green":
                    return Colours.Green;
                case "Yellow":
                    return Colours.Yellow;
                case "Brown":
                    return Colours.Brown;
                case "Orange":
                    return Colours.Orange;
                case "Pink":
                    return Colours.Pink;
                case "Purple":
                    return Colours.Purple;
                case "Grey":
                    return Colours.Grey;
                case "Teal":
                    return Colours.Teal;
                case "Cream":
                    return Colours.Cream;
                default:
                    throw new ArgumentException("Invalid Colour provided as parameter");
            }
        }

        private static OutfitCategories ParseOutfitCategoriesToEnum(string category)
        {
            switch (category)
            {
                case "Suits":
                    return OutfitCategories.Suits;
                case "Waistcoats":
                    return OutfitCategories.Waistcoats;
                case "Neckwear":
                    return OutfitCategories.Neckwear;
                case "Shirts":
                    return OutfitCategories.Shirts;
                case "Accessories":
                    return OutfitCategories.Accessories;
                default:
                    throw new ArgumentException("Invalid Outfit Category provided as parameter");
            }
        }

        #endregion

        #region FilterMethods

        private static IEnumerable<string> GetItemContainer(string outfitContainerXpath)
        {
            var outfitCount = WebBrowser.WebDriver.GetCountOfElements(string.Format("{0}//div", outfitContainerXpath));
            var itemContainer = new List<string>();

            for (var outfitIndex = 1; outfitIndex <= outfitCount; outfitIndex++)
            {
                itemContainer.Add(WebBrowser.WebDriver.FindElement(string.Format("{0}//div[{1}]", outfitContainerXpath, outfitIndex)).Text);
            }

            return itemContainer;
        }

        private static IEnumerable<string> GetColourContainer(string outfitContainerXpath)
        {
            var outfitCount = WebBrowser.WebDriver.GetCountOfElements(string.Format("{0}//li", outfitContainerXpath));
            var colourContainer = new List<string>();

            for (var outfitIndex = 1; outfitIndex <= outfitCount; outfitIndex++)
            {
                colourContainer.Add(WebBrowser.WebDriver.FindElement(string.Format("{0}//li[{1}]//span[2]", outfitContainerXpath, outfitIndex)).Text);
            }

            return colourContainer;
        }

        private static List<string> GetProperColourByKey(string colourKey)
        {
            return !string.IsNullOrEmpty(colourKey) ? Defaults.ColoursDictionary[colourKey].ToList() : Defaults.ColoursDictionary["All"].ToList();
        }

        private static List<string> GetProperStyleByKey(string styleKey)
        {
            if (string.IsNullOrEmpty(styleKey))
            {
                return Defaults.ColoursDictionary["All"].ToList();
            }

            var allStylesVariants = new List<string>();
            var stylesContainer = styleKey.Split(',');
            foreach (var styleVariant in stylesContainer)
            {
                allStylesVariants.AddRange(Defaults.StylesDictionary[styleVariant].ToList());
            }

            return allStylesVariants;
        }

        public bool CheckIfSuitsHaveBeenFiltered(string colour, string style)
        {
            NavigateToOutfitCategory(Defaults.OutfitCategorySuits);

            var itemContainer = GetItemContainer(Controls.divItemContainer.ControlInformation.Xpath);
            var isAllFilteredProperlyFlag = true;
            var allColourVariants = GetProperColourByKey(colour);
            var allStylesVariants = GetProperStyleByKey(style);

            foreach (var item in itemContainer)
            {
                var isItemFilteredProperlyFlag = allColourVariants.Any(colourVariant => item.StartsWith(colourVariant)) && allStylesVariants.Any(styleVariant => item.EndsWith(styleVariant));
                if (isItemFilteredProperlyFlag) continue;
                isAllFilteredProperlyFlag = false;
                break;
            }

            return isAllFilteredProperlyFlag;
        }

        public bool CheckIfWaistcoatsHaveBeenFiltered(string colour)
        {
            NavigateToOutfitCategory(Defaults.OutfitCategoryWaistcoats);

            var itemContainer = GetItemContainer(Controls.divItemContainerWaistcoats.ControlInformation.Xpath);
            var isAllFilteredProperlyFlag = true;
            var allColourVariants = GetProperColourByKey(colour);

            foreach (var item in itemContainer)
            {
                var isItemFilteredProperlyFlag = allColourVariants.Any(colourVariant => item.StartsWith(colourVariant));
                if (isItemFilteredProperlyFlag) continue;
                isAllFilteredProperlyFlag = false;
                break;
            }

            return isAllFilteredProperlyFlag;
        }

        public bool CheckIfNeckwearHaveBeenFiltered(string colour)
        {
            NavigateToOutfitCategory(Defaults.OutfitCategoryNeckwear);

            var colourContainer = GetColourContainer(Controls.divItemContainerNeckwear.ControlInformation.Xpath);
            var isAllFilteredProperlyFlag = true;
            var allColourVariants = GetProperColourByKey(colour);

            foreach (var item in colourContainer)
            {
                var isItemFilteredProperlyFlag = allColourVariants.Any(colourVariant => item.EndsWith(colourVariant));
                if (isItemFilteredProperlyFlag) continue;
                isAllFilteredProperlyFlag = false;
                break;
            }

            return isAllFilteredProperlyFlag;
        }

        public bool CheckIfShirtsHaveBeenFiltered()
        {
            NavigateToOutfitCategory(Defaults.OutfitCategoryShirts);

            var itemContainer = GetItemContainer(Controls.divItemContainer.ControlInformation.Xpath);
            var isAllFilteredProperlyFlag = true;
            var allShirtTypes = Defaults.ShirtDictionary["Type"].ToList();
            var allShirtModels = Defaults.ShirtDictionary["Model"].ToList();

            foreach (var item in itemContainer)
            {
                var isItemFilteredProperlyFlag = allShirtTypes.Any(shirtType => item.StartsWith(shirtType)) && allShirtModels.Any(shirtModel => item.EndsWith(shirtModel));
                if (isItemFilteredProperlyFlag) continue;
                isAllFilteredProperlyFlag = false;
                break;
            }

            return isAllFilteredProperlyFlag;
        }

        public bool CheckIfAccessoriesHaveBeenFiltered()
        {
            NavigateToOutfitCategory(Defaults.OutfitCategoryAccessories);

            var itemContainer = GetItemContainer(Controls.divItemContainer.ControlInformation.Xpath);
            var isAllFilteredProperlyFlag = true;
            var allAvailableAccessories = Defaults.AccessoriesDictionary["All"].ToList();

            foreach (var item in itemContainer)
            {
                var isItemFilteredProperlyFlag = allAvailableAccessories.Any(acc => item.StartsWith(acc));
                if (isItemFilteredProperlyFlag) continue;
                isAllFilteredProperlyFlag = false;
                break;
            }

            return isAllFilteredProperlyFlag;
        }

        public void ClearFilter(string category)
        {
            switch (ParseOutfitCategoriesToEnum(category))
            {
                case OutfitCategories.Suits:
                    Controls.divFiltersSuits.Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    WebBrowser.WebDriver.FindElement(
                        By.XPath(string.Format("{0}{1}", Controls.divFiltersSuits.ControlInformation.Xpath,
                            Controls.divClearFilter.ControlInformation.Xpath))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case OutfitCategories.Waistcoats:
                    Controls.divFiltersWaistcoats.Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    WebBrowser.WebDriver.FindElement(
                        By.XPath(string.Format("{0}{1}", Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                            Controls.divClearFilter.ControlInformation.Xpath))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case OutfitCategories.Neckwear:
                    Controls.divFiltersNeckwear.Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    WebBrowser.WebDriver.FindElement(
                        By.XPath(string.Format("{0}{1}", Controls.divFiltersNeckwear.ControlInformation.Xpath,
                            Controls.divClearFilter.ControlInformation.Xpath))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case OutfitCategories.Shirts:
                    break;
                case OutfitCategories.Accessories:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void ClearFilter()
        {
            Controls.divFiltersSuits.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
            WebBrowser.WebDriver.FindElement(
                By.XPath(string.Format("{0}{1}", Controls.divFiltersSuits.ControlInformation.Xpath,
                    Controls.divClearFilter.ControlInformation.Xpath))).Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            Controls.divFiltersWaistcoats.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
            WebBrowser.WebDriver.FindElement(
                By.XPath(string.Format("{0}{1}", Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                    Controls.divClearFilter.ControlInformation.Xpath))).Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            Controls.divFiltersNeckwear.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
            WebBrowser.WebDriver.FindElement(
                By.XPath(string.Format("{0}{1}", Controls.divFiltersNeckwear.ControlInformation.Xpath,
                    Controls.divClearFilter.ControlInformation.Xpath))).Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
        }

        public bool FilterBySuits()
        {
            Controls.divFiltersSuits.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return Controls.divSuitsContent.IsDisplayed;
        }

        private void FilterBySuitsColour(string colour)
        {
            if (!string.IsNullOrEmpty(colour))
            {
                switch (ParseColoursToEnum(colour))
                {
                    case Colours.Black:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersSuits.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Black))).Click();
                        break;
                    case Colours.Blue:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersSuits.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Blue))).Click();
                        break;
                    case Colours.Grey:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersSuits.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Grey))).Click();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
        }

        private void FilterBySuitsStyle(string styles)
        {
            if (string.IsNullOrEmpty(styles)) return;
            var styleContainer = styles.Split(',');

            foreach (var style in styleContainer)
            {
                switch (ParseOutfitStylesToEnum(style))
                {
                    case Styles.Tailcoat:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//input[@id='{2}']",
                                Controls.divFiltersSuits.ControlInformation.Xpath,
                                Controls.divFiltersStyles.ControlInformation.Xpath, (int)Styles.Tailcoat))).Click();
                        break;
                    case Styles.ShortJacket:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//input[@id='{2}']",
                                Controls.divFiltersSuits.ControlInformation.Xpath,
                                Controls.divFiltersStyles.ControlInformation.Xpath, (int)Styles.ShortJacket)))
                            .Click();
                        break;
                    case Styles.PrinceEdward:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//input[@id='{2}']",
                                Controls.divFiltersSuits.ControlInformation.Xpath,
                                Controls.divFiltersStyles.ControlInformation.Xpath, (int)Styles.PrinceEdward)))
                            .Click();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
        }

        public void FilterBySuits(string colour, string styles)
        {
            FilterBySuitsColour(colour);
            FilterBySuitsStyle(styles);
        }

        public bool FilterByWaistcoats()
        {
            Controls.divFiltersWaistcoats.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return Controls.divWaistcoatsContent.IsDisplayed;
        }

        public void FilterByWaistcoats(string colour)
        {
            ClearFilter(Defaults.OutfitCategoryWaistcoats);

            if (colour != string.Empty)
            {
                switch (ParseColoursToEnum(colour))
                {
                    case Colours.Black:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Black))).Click();
                        break;
                    case Colours.Blue:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Blue))).Click();
                        break;
                    case Colours.Red:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Red))).Click();
                        break;
                    case Colours.Green:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Green))).Click();
                        break;
                    case Colours.Yellow:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Yellow))).Click();
                        break;
                    case Colours.Brown:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Brown))).Click();
                        break;
                    case Colours.Orange:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Orange))).Click();
                        break;
                    case Colours.Pink:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Pink))).Click();
                        break;
                    case Colours.Purple:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Purple))).Click();
                        break;
                    case Colours.Grey:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Grey))).Click();
                        break;
                    case Colours.Teal:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Teal))).Click();
                        break;
                    case Colours.Cream:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersWaistcoats.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Cream))).Click();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
        }

        public bool FilterByNeckwear()
        {
            Controls.divFiltersNeckwear.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return Controls.divNeckwearContent.IsDisplayed;
        }

        public void FilterByNeckwear(string colour)
        {
            ClearFilter(Defaults.OutfitCategoryNeckwear);

            if (colour != string.Empty)
            {
                switch (ParseColoursToEnum(colour))
                {
                    case Colours.Black:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Black))).Click();
                        break;
                    case Colours.Blue:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Blue))).Click();
                        break;
                    case Colours.Red:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Red))).Click();
                        break;
                    case Colours.Green:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Green))).Click();
                        break;
                    case Colours.Yellow:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Yellow))).Click();
                        break;
                    case Colours.Brown:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Brown))).Click();
                        break;
                    case Colours.Orange:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Orange))).Click();
                        break;
                    case Colours.Pink:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Pink))).Click();
                        break;
                    case Colours.Purple:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Purple))).Click();
                        break;
                    case Colours.Grey:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Grey))).Click();
                        break;
                    case Colours.Teal:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Teal))).Click();
                        break;
                    case Colours.Cream:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.Cream))).Click();
                        break;
                    case Colours.White:
                        WebBrowser.WebDriver.FindElement(
                            By.XPath(string.Format("{0}{1}//li[@data-colourfamilyid='{2}']",
                                Controls.divFiltersNeckwear.ControlInformation.Xpath,
                                Controls.divFiltersColours.ControlInformation.Xpath, (int)Colours.White))).Click();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
        }

        public bool FilterByShirts()
        {
            Controls.divFiltersShirts.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return Controls.divShirtsContent.IsDisplayed;
        }

        public bool FilterByAccessories()
        {
            Controls.divFiltersAccessories.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return Controls.divAccessoriesContent.IsDisplayed;
        }

        #endregion

        #region NavigationMethods

        private void InitializeOutfitCategories()
        {
            _outfitCategoriesList.Clear();
            _outfitCategoriesList.Add(Controls.divSuitsContent);
            _outfitCategoriesList.Add(Controls.divWaistcoatsContent);
            _outfitCategoriesList.Add(Controls.divNeckwearContent);
            _outfitCategoriesList.Add(Controls.divShirtsContent);
            _outfitCategoriesList.Add(Controls.divAccessoriesContent);
        }

        private int ReadActiveCategory()
        {
            InitializeOutfitCategories();

            return _outfitCategoriesList.FindIndex(category => category.IsDisplayed);
        }

        public void NavigateToOutfitCategory(string category)
        {
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
            var iterator = ReadActiveCategory();

            while (true)
            {
                InitializeOutfitCategories();
                if (_outfitCategoriesList[iterator].ControlInformation.Name.Contains(category)) return;
                WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/div[3]", _outfitCategoriesList[iterator].ControlInformation.Xpath))).Click();
                WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                if (++iterator == _outfitCategoriesList.Count)
                    iterator = 0;
            }
        }

        public bool CheckIfUserHasBeenRedirectedToOutfitBuilderPage()
        {
            return WebBrowser.WebDriver.Title.Contains(PageInformation.Title);
        }

        public void ClickOnViewOutfitSummary()
        {
            Controls.btnViewOutfitSummary.Click();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
            WaitForLoad();
        }

        #endregion

        #region RecommendedOutfitMethods

        public bool CheckSelectedGreySuitsForFifityShadesOfGrey()
        {
            return Controls.GreyColourOfSuit.IsDisplayed;
        }

        public bool CheckSelectedRucheForColourMeBold()
        {
            return Controls.selectedRuche.IsDisplayed;
        }

        public bool CheckLoadedPartOfClothes()
        {
            var loadedImage = false;

            foreach (var partOfClothes in _checkImage)
            {
                loadedImage = partOfClothes.Equals(true);
            }
            return loadedImage;
        }

        public bool CheckImageOfTheBlackTailcoat()
        {
            _checkImage.Add(Controls.imgBlackHerringboneTailcoat.IsDisplayed);
            _checkImage.Add(Controls.imgGreyStripeTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgLovesFirstBlushRuche.IsDisplayed);
            _checkImage.Add(Controls.imgDoveGreyDoubleBreastedWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheGreySilkTailCoat()
        {
            _checkImage.Add(Controls.imgGreySilkTailcoat.IsDisplayed);
            _checkImage.Add(Controls.imgGreySilkTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgPistachioTie.IsDisplayed);
            _checkImage.Add(Controls.imgPistachioWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheBlackSlimFit()
        {
            _checkImage.Add(Controls.imgBlackSlimFitShort.IsDisplayed);
            _checkImage.Add(Controls.imgBlackSlimFitTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgPurpleStormTie.IsDisplayed);
            _checkImage.Add(Controls.imgPurpleStormWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheNavyPrinceEdward()
        {
            _checkImage.Add(Controls.imgNavyHerringbonePrinceEdward.IsDisplayed);
            _checkImage.Add(Controls.imgNavyStripeTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgPomegranateRuche.IsDisplayed);
            _checkImage.Add(Controls.imgPomegranateWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheFirstOutfit()
        {
            _checkImage.Add(Controls.imgMidGreyTailcoat.IsDisplayed);
            _checkImage.Add(Controls.imgMidGreyTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgLovesFirstBlushRuche.IsDisplayed);
            _checkImage.Add(Controls.imgMidGreyWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheSecondOutfit()
        {
            _checkImage.Add(Controls.imgBlackHerringboneTailcoat.IsDisplayed);
            _checkImage.Add(Controls.imgGreyStripeTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgNavyRuche.IsDisplayed);
            _checkImage.Add(Controls.imgDoveGreyDoubleBreastedWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheThirdOutfit()
        {
            _checkImage.Add(Controls.imgGreySilkShort.IsDisplayed);
            _checkImage.Add(Controls.imgGreySilkTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgFuschiaTie.IsDisplayed);
            _checkImage.Add(Controls.imgGreySilkWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheFourthOutfit()
        {
            _checkImage.Add(Controls.imgBlackSlimFitShort.IsDisplayed);
            _checkImage.Add(Controls.imgBlackSlimFitTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgClaretTie.IsDisplayed);
            _checkImage.Add(Controls.imgBlackSlimFitWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheFifthOutfit()
        {
            _checkImage.Add(Controls.imgBlackHerringboneTailcoat.IsDisplayed);
            _checkImage.Add(Controls.imgGreyStripeTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgLovesFirstBlushRuche.IsDisplayed);
            _checkImage.Add(Controls.imgDoveGreyDoubleBreastedWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        public bool CheckImageOfTheSixthOutfit()
        {
            _checkImage.Add(Controls.imgSlateGreyPrinceEdward.IsDisplayed);
            _checkImage.Add(Controls.imgSlateGreyTrousers.IsDisplayed);
            _checkImage.Add(Controls.imgBerryRuche.IsDisplayed);
            _checkImage.Add(Controls.imgIvoryWaistcoat.IsDisplayed);

            return CheckLoadedPartOfClothes();
        }

        #endregion
    }
}
