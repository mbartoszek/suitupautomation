﻿using System;
using OpenQA.Selenium;

namespace Automation.Core.Pages
{
    public partial class MainPage
    {
        public enum CarouselIndicators
        {
            OutfitBuilder = 1,
            InspireMe,
            GroomGoesFree,
            BookTryOn
        }

        private static CarouselIndicators ParseCarouselIndicatorsToEnum(string indicator)
        {
            switch (indicator)
            {
                case "Outfit Builder":
                    return CarouselIndicators.OutfitBuilder;
                case "Inpire Me":
                    return CarouselIndicators.InspireMe;
                case "Groom Goes Free":
                    return CarouselIndicators.GroomGoesFree;
                case "Free Try On":
                    return CarouselIndicators.BookTryOn;
                default:
                    throw new ArgumentException("Carousel Indicator does not match with any existing one");
            }
        }

        public OutfitBuilderPage ClickOnOutfitBuilderLink()
        {
            Controls.lnkOutfitBuilder.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public InspireMePage ClickOnInspireMeLink()
        {
            Controls.lnkInspireMe.Click();
            WaitForLoad();

            return new InspireMePage();
        }

        public OutfitBuilderPage ClickOnViewOutFitsButtonOfFiftyShadesOfGrey()
        {
            Controls.btnViewOutfitsInFiftyShadesOfGrey.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnTheFiftyShadesOfGreyLink()
        {
            Controls.lnkViewOutfitsInFiftyShadesOfGrey.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnViewOutfitsButtonOfColourMeBold()
        {
            Controls.btnViewOutfitsInColourMeBold.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnViewTheColourMeBoldLink()
        {
            Controls.lnkViewOutfitsInColourMeBold.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnBlackTailcoatLink()
        {
            Controls.lnkBlackTailcoat.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnViewOutfitButtonOfTheBlackTailcoat()
        {
            Controls.btnBlackTailcoat.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnGreySilkTailcoatLink()
        {
            Controls.lnkGreySilkTailcoat.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnViewOutfitButtonOfTheGreySilkTailcoat()
        {
            Controls.btnGreySilkTailcoat.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnBlackSlimFitLink()
        {
            Controls.lnkBlackSlimFit.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnViewOutfitButtonOfTheBlackSlimFit()
        {
            Controls.btnBlackSlimFit.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnNavyPrinceEdnwardLink()
        {
            Controls.lnkNavyPrinceEdward.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public OutfitBuilderPage ClickOnViewOutfitButtonOfTheNavyPrinceEdward()
        {
            Controls.btnNavyPrinceEdward.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public void ChangeViewOnCarousel(string indicator)
        {
            if (!Controls.spanPauseCarousel.IsDisplayed)
                Controls.spanPauseCarousel.Click();

            var carouselIndicatorsXpath = Controls.divCarouselIndicators.ControlInformation.Xpath;

            switch (ParseCarouselIndicatorsToEnum(indicator))
            {
                case CarouselIndicators.OutfitBuilder:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.OutfitBuilder))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.InspireMe:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.InspireMe))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.GroomGoesFree:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.GroomGoesFree))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                case CarouselIndicators.BookTryOn:
                    WebBrowser.WebDriver.FindElement(By.XPath(string.Format("{0}/li[{1}]", carouselIndicatorsXpath, (int)CarouselIndicators.BookTryOn))).Click();
                    WebBrowser.WebDriver.WaitUntilJQueryIsComplete();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public OutfitBuilderPage ClickOnGetStartedButton()
        {
            Controls.btnGetStarted.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new OutfitBuilderPage();
        }

        public GroomGoesFreePage ClickOnFindOutMoreGroomButton()
        {
            Controls.btnFindOutMore.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new GroomGoesFreePage();
        }

        public BookTryOnPage ClickOnFindOutMoreTryOnButton()
        {
            Controls.btnFindOutMore.Click();
            WaitForLoad();
            WebBrowser.WebDriver.WaitUntilJQueryIsComplete();

            return new BookTryOnPage();
        }

        public HowItWorksPage ClickOnHowItWorksLink()
        {
            Controls.lnkHowItWorks.Click();
            WaitForLoad();

            return new HowItWorksPage();
        }

        public SubscribePage ClickOnSubscribeButton()
        {
            Controls.btnSubscribe.WebElement.Submit();
            WaitForLoad();

            return new SubscribePage();
        }

        public TermsConditionsPage ClickOnTermsConditionsLink()
        {
            Controls.lnkTermsConditions.Click();
            WaitForLoad();

            return new TermsConditionsPage();
        }

        public CookiesPrivacyPage ClickOnCookiesPrivacyLink()
        {
            Controls.lnkCookiesPrivacy.Click();
            WaitForLoad();

            return new CookiesPrivacyPage();
        }

        public HelpFAQsPage ClickOnHelpFAQsLink()
        {
            Controls.lnkHelpFAQs.Click();
            WaitForLoad();

            return new HelpFAQsPage();
        }

        public PriceListPage ClickOnPriceListLink()
        {
            Controls.lnkPriceList.Click();
            WaitForLoad();

            return new PriceListPage();
        }

        public XedoPage ClickOnXedoLink()
        {
            Controls.lnkXedo.Click();
            WaitForLoad();

            return new XedoPage();
        }

        public MainPage ClickOnSignInLink()
        {
            Controls.lnkSignIn.Click();
            WaitForLoad();

            return new MainPage();
        }

        public MainPage EnterUsername(string username)
        {
            Controls.tbxUsername.Clear();
            Controls.tbxUsername.SendKeys(username);
            WaitForLoad();

            return new MainPage();
        }

        public bool CheckUsername()
        {
            return Controls.divUsernameValidation.IsDisplayed;
        }

        public MainPage EnterPassword(string password)
        {
            Controls.tbxPassword.Clear();
            Controls.tbxPassword.SendKeys(password);
            WaitForLoad();

            return new MainPage();
        }

        public bool CheckPassword()
        {
            return Controls.divPasswordValidation.IsDisplayed;
        }

        public bool CheckIfUserIsLogged()
        {
            return Controls.lnkLogout.IsDisplayed;
        }

        public OrdersPage ClickOnLoginInButton()
        {
            Controls.btnLoginIn.Click();
            WaitForLoad();

            return new OrdersPage();
        }
    }
}
