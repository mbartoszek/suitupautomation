﻿using Automation.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Automation.Core
{
    [Serializable]
    public class ControlDetail 
    {
        #region Constants

        private const string ControlNamePrefix = "Automation.Core.Controls.";
        private const string NameNode = "name";
        private const string TypeNode = "type";
        private const string XpathNode = "xpath";

        #endregion

        #region Fields

        private string _controlType;
        private Dictionary<string, ControlDetail> _controlDetails;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlDetail"/> class.
        /// </summary>
        public ControlDetail()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlDetail" /> class.
        /// </summary>
        /// <param name="control">The control.</param>
        public ControlDetail(IXPathNavigable control)
        {
            try
            {
                var controlNavigator = control.CreateNavigator();
                Name = controlNavigator.GetAttribute(NameNode, string.Empty);
                ControlType = controlNavigator.GetAttribute(TypeNode, string.Empty);
                Xpath = controlNavigator.GetAttribute(XpathNode, string.Empty);
            }
            catch (NullReferenceException exception)
            {           
                throw new IncorrectXmlDataException("One of attributes in control element does not exist.", exception);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the control details dictionary.
        /// </summary>
        /// <value>
        /// The control details.
        /// </value>
        [XmlIgnore]
        public Dictionary<string, ControlDetail> ControlDetails
        {
            get {
                return _controlDetails ??
                       (_controlDetails =
                        ControlHelper.CreateControlDetails(ObjectHelper.Clone(ControlsDetailsList), ref _controlType,
                                                           Name, null));
            }
        }

        /// <summary>
        /// Gets or sets the name of control.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute(NameNode)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the control.
        /// </summary>
        /// <value>
        /// The type of the control.
        /// </value>
        [XmlAttribute(TypeNode)]
        public string ControlType
        {
            get
            {
                return string.Concat(ControlNamePrefix, _controlType);
            }

            set
            {
                _controlType = value;
            }
        }

        /// <summary>
        /// Gets or sets the control xpath.
        /// </summary>
        /// <value>
        /// The xpath.
        /// </value>
        [XmlAttribute(XpathNode)]
        public string Xpath { get; set; }

        /// <summary>
        /// Gets or sets the controls details list.
        /// </summary>
        /// <value>
        /// The controls details list.
        /// </value>
        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Collection is deserialized from xml so cannot be read only."), XmlArray("Controls")]
        [XmlArrayItem("Control", typeof(ControlDetail))]
        public Collection<ControlDetail> ControlsDetailsList { get; set; }

        #endregion
    }
}
