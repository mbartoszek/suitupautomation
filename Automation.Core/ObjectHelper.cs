﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Automation.Core
{
    /// <summary>
    /// Class with functions to help with basic operations.
    /// </summary>
    public static class ObjectHelper
    {
        #region Methods

        /// <summary>
        /// Clones the object.
        /// </summary>
        /// <typeparam name="T">Type of the object.</typeparam>
        /// <param name="objectToClone">Object to clone.</param>
        /// <returns>Clone object.</returns>
        public static T Clone<T>(T objectToClone)
        {
            using (var memoryStream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, objectToClone);
                memoryStream.Position = 0;

                return (T)formatter.Deserialize(memoryStream);
            }
        }

        #endregion
    }
}
