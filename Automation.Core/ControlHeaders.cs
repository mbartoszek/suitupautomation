﻿using Automation.Core.Exceptions;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using Automation.Configuration;

namespace Automation.Core
{
    [Serializable]
    public class ControlHeaders 
    {
        #region Constants

        private const string NameNode = "name";
        private const string HeaderNode = "header";
        private const string TableNameNode = "tablename";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlHeaders"/> class.
        /// </summary>
        public ControlHeaders()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlHeaders" /> class.
        /// </summary>
        /// <param name="header">The control.</param>
        public ControlHeaders(IXPathNavigable header)
        {
            try
            {
                var headerNavigator = header.CreateNavigator();
                Name = headerNavigator.GetAttribute(NameNode, string.Empty);
                Header = headerNavigator.GetAttribute(HeaderNode, string.Empty);
                TableName = headerNavigator.GetAttribute(TableNameNode, string.Empty);
            }
            catch (NullReferenceException exception)
            {           
                throw new IncorrectXmlDataException("One of attributes in control element does not exist.", exception);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of control.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [XmlAttribute(NameNode)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the page where is control placed.
        /// </summary>
        /// <value>
        /// The name of the page where is control placed.
        /// </value>
        [XmlAttribute(TableNameNode)]
        public string TableName { get; set; }

        /// <summary>
        /// Gets or sets the control xpath.
        /// </summary>
        /// <value>
        /// The xpath.
        /// </value>
        [XmlAttribute(HeaderNode)]
        public string Header { get; set; }

        /// <summary>
        /// Gets or sets the controls details list.
        /// </summary>
        /// <value>
        /// The controls details list.
        /// </value>
        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Collection is deserialized from xml so cannot be read only."), XmlArray("Headers")]
        [XmlArrayItem("Header", typeof(ControlHeaders))]
        public Collection<ControlHeaders> ControlHeadersList { get; set; }

        #endregion
    }
}
