﻿using Automation.Core.Controls;
using Automation.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Automation.Core
{
    public static class ControlFactory
    {
        #region Delegates

        private delegate Control ControlCreator(ControlDetail controlDetails, int timeout);
        private delegate Control ControlCreatorWithList(ControlDetail controlDetails, int timeout, Dictionary<string, ControlHeaders> controlHeaders);

        private delegate dynamic DerivedControlCreator(Control control);

        #endregion

        #region Methods

        /// <summary>
        /// Creates the control.
        /// </summary>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created control object of type declared in file.
        /// </returns>
        /// <exception cref="ControlCreationException">Exception indicate that declared in Xml file type of control does not exist.</exception>
        public static dynamic CreateControl(ControlDetail controlDetails, int timeout)
        {
            var type = Type.GetType(controlDetails.ControlType);

            if (type == null)
                throw new ControlCreationException(string.Format("Control type '{0}' does not exists. Cannot create control '{1} of that type.", controlDetails.ControlType, controlDetails.Name));

            var methodInfo = type.GetMethod("Create");
            
            var createInstanceDelegate = Delegate.CreateDelegate(typeof(ControlCreator), methodInfo) as ControlCreator;
            
            return createInstanceDelegate(controlDetails, timeout);
        }

        public static dynamic CreateControl(ControlDetail controlDetails, int timeout, Dictionary<string, ControlHeaders> controlHeaders)
        {
            var type = Type.GetType(controlDetails.ControlType);

            if (type == null)
                throw new ControlCreationException(string.Format("Control type '{0}' does not exists. Cannot create control '{1} of that type.", controlDetails.ControlType, controlDetails.Name));

            var methodInfo = type.GetMethod("Create");

            var createInstanceDelegate = Delegate.CreateDelegate(typeof(ControlCreatorWithList), methodInfo) as ControlCreatorWithList;

            return createInstanceDelegate(controlDetails, timeout, controlHeaders);
        }

        /// <summary>
        /// Creates the control.
        /// </summary>
        /// <typeparam name="T">Expected type of control.</typeparam>
        /// <param name="control">The control.</param>
        /// <returns>
        /// Created control object of type T.
        /// </returns>
        private static T CreateControl<T>(Control control)
        {
            MethodInfo methodInfo = typeof(T).GetMethod("CreateByControl");
            var createInstanceDelegate = Delegate.CreateDelegate(typeof(DerivedControlCreator), methodInfo) as DerivedControlCreator;
            
            return (T)createInstanceDelegate(control);
        }

        /// <summary>
        /// Creates the control.
        /// </summary>
        /// <typeparam name="T">Expected type of control.</typeparam>
        /// <param name="controlDetails">The control details.</param>
        /// <param name="timeout">The control load timeout.</param>
        /// <returns>
        /// Created control object of type T.
        /// </returns>
        public static T CreateControl<T>(ControlDetail controlDetails, int timeout) where T : class 
        {
            var control = CreateControl(controlDetails, timeout);

            var newControl = control as T;
            if (newControl != null)
                return newControl;

            if (typeof(T).IsSubclassOf(control.GetType()))
                return CreateControl<T>(control);

            throw new ControlCreationException(string.Format("Cannot create object '{0}' of type '{1}'. It's not correct control type.", controlDetails.Name, typeof(T).Name));
        }

        public static T CreateControl<T>(ControlDetail controlDetails, int timeout, Dictionary<string, ControlHeaders> controlHeaders) where T : class
        {
            var control = CreateControl(controlDetails, timeout, controlHeaders);

            var newControl = control as T;
            if (newControl != null)
                return newControl;

            if (typeof(T).IsSubclassOf(control.GetType()))
                return CreateControl<T>(control);

            throw new ControlCreationException(string.Format("Cannot create object '{0}' of type '{1}'. It's not correct control type.", controlDetails.Name, typeof(T).Name));
        }

        #endregion
    }
}
