﻿Feature: CheckPageTitle
	As a system
	I want to go to all pages
	In order to ensure that all titles of the pages are properly

@TitleOfTheBuildPage
Scenario: Verify the title of the Build Page
	When I go to Build Page 
	Then ensure that the title of the Build page is displayed properly

	@TitleOfTheInspireMePage
Scenario: Verify the title of the Inspire Me Page
	When I go to Inspire Me Page
	Then ensure that the title of the Inspire Me page is displayed properly

	@TitleOfTheGroomGoesFreePage
	Scenario: Verify the title of the Groom Goes Free Page
	Given I have proceed to Groom Goes Free menu on Carousel
	When I go to Groom Goes Free Page
	Then ensure that the title of the Groom Goes Free page is displayed properly

	@TitleOfTheBookTryOnPage
	Scenario: Verify the title of the Book Try On Page
	Given I have proceed to Free Try On menu on Carousel
	When I go to Book Try On Page
	Then ensure that the title of the Book Try On page is displayed properly

	@TitleOfTheHowItWorksPage
Scenario: Verify the title of the How It Works Page
	When I go to How It Works Page
	Then ensure that the title of the  How It Works page is displayed properly

	@TitleOfTheAccountPage
	Scenario: Verify the title of the Account Page
	Given I have opened the navigation menu
	When I click on Profile link on navigation menu
	Then ensure that the title of the Account page is displayed properl

	@TitleOfTheSubscribePage
Scenario: Verify the title of the Subscribe Page
	When I go to Subscribe Page
	Then ensure that the title of the Subscribe page is displayed properly

	@TitleOfTheTermsConditionsPage
Scenario: Verify the title of the Terms Conditions Page
	When I go to Terms Conditions Page
	Then ensure that the title of the Terms Conditions page is displayed properly

	@TitleOfTheCookiesPrivacyPage
Scenario: Verify the title of the Cookies Privacy Page
	When I go to Cookies Privacy Page
	Then ensure that the title of the  Cookies Privacy page is displayed properly

	@TitleOfTheHelpFAQsPage
Scenario: Verify the title of the Help FAQs Page
	When I go to HelpFAQs Page
	Then ensure that the title of the Help FAQs page is displayed properly

	@TitleOfThePriceListPage
Scenario: Verify the title of the Price List Page
	When I go to Price List Page
	Then ensure that the title of the Price List page is displayed properly

	@TitleOfTheXedoPage
Scenario: Verify the title of the Xedo Page
	When I go to Xedo Page	
	Then ensure that the title of the Xedo page is displayed properly				

