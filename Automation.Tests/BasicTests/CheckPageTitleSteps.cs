﻿using System;
using Automation.Core.Pages;
using Automation.Configuration;
using Automation.Tests.TestInitializers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Tests.BasicTests
{
    [Binding]
    public class CheckPageTitleSteps : TestInitializer
    {
        private string invalidTitle = "The title of the page is properly";

        #region TitleOfTheBuildPage

        [When(@"I go to Build Page")]
        public void WhenIGoToBuildPage()
        {
            var outfitBuilderPage = MainPage.ClickOnOutfitBuilderLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"ensure that the title of the Build page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheBuildPageIsDisplayedProperly()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.AreEqual(Defaults.OutFitBuilderTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheInspireMePage

        [When(@"I go to Inspire Me Page")]
        public void WhenIGoToInspireMePage()
        {
            var inspireMePage = MainPage.ClickOnInspireMeLink();

            ScenarioContext.Current.Add("InspireMeLinkPage", inspireMePage);
        }

        [Then(@"ensure that the title of the Inspire Me page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheInspireMePageIsDisplayedProperly()
        {
            var inspireMePage = new InspireMePage();
            ScenarioContext.Current.TryGetValue("InspireMeLinkPage", out inspireMePage);

            Assert.AreEqual(Defaults.InspireMeTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheHowItWorksPage

        [When(@"I go to How It Works Page")]
        public void WhenIGoToHowItWorksPage()
        {
            var howItWorksPage = MainPage.ClickOnHowItWorksLink();

            ScenarioContext.Current.Add("HowItWorksPage", howItWorksPage);
        }

        [Then(@"ensure that the title of the  How It Works page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheHowItWorksPageIsDisplayedProperly()
        {
            var howItWorksPage = new HowItWorksPage();
            ScenarioContext.Current.TryGetValue("HowItWorksPage", out howItWorksPage);

            Assert.AreEqual(Defaults.HowItWorksTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheAccountPage

       
        [Given(@"I have proceed to Profile menu on Carousel")]
        public void GivenIHaveProceedToProfileMenuOnCarousel()
        {
            var navigationPage = MainPage.ClickOnNavigationBurger<NavigationPage>();

            ScenarioContext.Current.Add("NavigationPage", navigationPage);
        }

        [When(@"I click on Profile link on navigation menu")]
        public void WhenIClickOnProfileLinkOnNavigationMenu()
        {
            var navigationPage = new NavigationPage();
            ScenarioContext.Current.TryGetValue("NavigationPage", out navigationPage);

            var accountPage = navigationPage.ChooseOptionFromNavigationMenu<AccountPage>(Defaults.NavigationItemProfile);

            ScenarioContext.Current.Add("AccountPage", accountPage);
        }

        [Then(@"ensure that the title of the Account page is displayed properl")]
        public void ThenEnsureThatTheTitleOfTheAccountPageIsDisplayedProperl()
        {
            var accountPage = new AccountPage();
            ScenarioContext.Current.TryGetValue("AccountPage", out accountPage);

            Assert.AreEqual(Defaults.AccountTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheSubscribePage

       
        [When(@"I go to Subscribe Page")]
        public void WhenIGoToSubscribePage()
        {
            var navigationPage = new NavigationPage();
            ScenarioContext.Current.TryGetValue("NavigationPage", out navigationPage);

            var subscribePage = MainPage.ClickOnSubscribeButton();

            ScenarioContext.Current.Add("SubscribePage", subscribePage);
        }

        [Then(@"ensure that the title of the Subscribe page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheSubscribePageIsDisplayedProperly()
        {
            var subscribePage = new SubscribePage();
            ScenarioContext.Current.TryGetValue("SubscribePage", out subscribePage);
            CurrentTab();

            Assert.AreEqual(Defaults.SubscribeTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheTermsConditionsPage

        [When(@"I go to Terms Conditions Page")]
        public void WhenIGoToTermsConditionsPage()
        {
            var termsConditionsPage = MainPage.ClickOnTermsConditionsLink();

            ScenarioContext.Current.Add("TermsConditionsPage", termsConditionsPage);
        }

        [Then(@"ensure that the title of the Terms Conditions page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheTermsConditionsPageIsDisplayedProperly()
        {
            var termsConditionsPage = new TermsConditionsPage();
            ScenarioContext.Current.TryGetValue("TermsConditionsPage", out termsConditionsPage);

            Assert.AreEqual(Defaults.TermsConditionsTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheCookiesPrivacyPage

        [When(@"I go to Cookies Privacy Page")]
        public void WhenIGoToCookiesPrivacyPage()
        {
            var cookiesPrivacyPage = MainPage.ClickOnCookiesPrivacyLink();

            ScenarioContext.Current.Add("CookiesPrivacyPage", cookiesPrivacyPage);
        }

        [Then(@"ensure that the title of the  Cookies Privacy page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheCookiesPrivacyPageIsDisplayedProperly()
        {
            var cookiesPrivacyPage = new CookiesPrivacyPage();
            ScenarioContext.Current.TryGetValue("CookiesPrivacyPage", out cookiesPrivacyPage);

            Assert.AreEqual(Defaults.CookiesPrivacyTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheHelpFAQsPage

        [When(@"I go to HelpFAQs Page")]
        public void WhenIGoToHelpFAQsPage()
        {
            var helpFAQsPage = MainPage.ClickOnHelpFAQsLink();

            ScenarioContext.Current.Add("HelpFAQsPage", helpFAQsPage);
        }

        [Then(@"ensure that the title of the Help FAQs page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheHelpFAQsPageIsDisplayedProperly()
        {
            var helpFAQsPage = new HelpFAQsPage();
            ScenarioContext.Current.TryGetValue("HelpFAQsPage", out helpFAQsPage);

            Assert.AreEqual(Defaults.HelpFAQsTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfThePriceListPage

        [When(@"I go to Price List Page")]
        public void WhenIGoToPriceListPage()
        {
            var priceListPage = MainPage.ClickOnPriceListLink();

            ScenarioContext.Current.Add("PriceListPage", priceListPage);
        }

        [Then(@"ensure that the title of the Price List page is displayed properly")]
        public void ThenEnsureThatTheTitleOfThePriceListPageIsDisplayedProperly()
        {
            var priceListPage = new PriceListPage();
            ScenarioContext.Current.TryGetValue("PriceListPage", out priceListPage);

            Assert.AreEqual(Defaults.PriceListTitle, CurrentTitleOfThePage(), invalidTitle); ;
        }

        #endregion

        #region TitleOfTheXedoPage

        [When(@"I go to Xedo Page")]
        public void WhenIGoToXedoPage()
        {
            var xedoPage = MainPage.ClickOnXedoLink();

            ScenarioContext.Current.Add("xedoPage", xedoPage);
        }

        [Then(@"ensure that the title of the Xedo page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheXedoPageIsDisplayedProperly()
        {
            var xedoPage = new XedoPage();
            ScenarioContext.Current.TryGetValue("XedoPage", out xedoPage);

            Assert.AreEqual(Defaults.XedoTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheGroomGoesFreePage

        [When(@"I go to Groom Goes Free Page")]
        public void WhenIGoToGroomGoesFreePage()
        {
            var groomGoesFreePage = MainPage.ClickOnFindOutMoreGroomButton();

            ScenarioContext.Current.Add("GroomGoesFreePage", groomGoesFreePage);
        }

        [Then(@"ensure that the title of the Groom Goes Free page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheGroomGoesFreePageIsDisplayedProperly()
        {
            var groomGoesFreePage = new GroomGoesFreePage();
            ScenarioContext.Current.TryGetValue("GroomGoesFreePage", out groomGoesFreePage);

            Assert.AreEqual(Defaults.GroomGoesFreeTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion

        #region TitleOfTheBookTryOnPage

        [When(@"I go to Book Try On Page")]
        public void WhenIGoToBookTryOnPage()
        {
            var bookTryOnPage = MainPage.ClickOnFindOutMoreTryOnButton();

            ScenarioContext.Current.Add("BookTryOnPage", bookTryOnPage);
        }

        [Then(@"ensure that the title of the Book Try On page is displayed properly")]
        public void ThenEnsureThatTheTitleOfTheBookTryOnPageIsDisplayedProperly()
        {
            var bookTryOnPage = new BookTryOnPage();
            ScenarioContext.Current.TryGetValue("BookTryOnPage", out bookTryOnPage);

            Assert.AreEqual(Defaults.BookTryOnTitle, CurrentTitleOfThePage(), invalidTitle);
        }

        #endregion
    }
}
