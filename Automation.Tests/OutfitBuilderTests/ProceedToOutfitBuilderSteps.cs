﻿using Automation.Configuration;
using Automation.Core.Pages;
using Automation.Tests.TestInitializers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Tests.OutfitBuilderTests
{
    public class ProceedToOutfitBuilderSteps : TestInitializer
    {
        #region OutfitBuilderLink

        [When(@"I click on Outfit Builder link")]
        public void WhenIClickOnOutfitBuilderLink()
        {
            var outfitBuilderPage = MainPage.ClickOnOutfitBuilderLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"I should be redirected to Outfit Builder page")]
        public void ThenIShouldBeRedirectedToOutfitBuilderPage()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckIfUserHasBeenRedirectedToOutfitBuilderPage(), "Outfit Builder Page has not been loaded correctly");
        }

        #endregion

        #region OutfitBuilderBurger

        [Given(@"I have opened the navigation menu")]
        public void GivenIHaveOpenedTheNavigationMenu()
        {
            var navigationPage = MainPage.ClickOnNavigationBurger<NavigationPage>();

            ScenarioContext.Current.Add("NavigationPage", navigationPage);
        }

        [When(@"I click on Outfit Builder link on navigation menu")]
        public void WhenIClickOnOutfitBuilderLinkOnNavigationMenu()
        {
            var navigationPage = new NavigationPage();
            ScenarioContext.Current.TryGetValue("NavigationPage", out navigationPage);

            var outfitBuilderPage = navigationPage.ChooseOptionFromNavigationMenu<OutfitBuilderPage>(Defaults.NavigationItemOutfitBuilder);

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region OutfitBuilderCarousel

        [Given(@"I have proceed to Outfit Builder menu on Carousel")]
        public void GivenIHaveProceedToOutfitBuilderMenuOnCarousel()
        {
            MainPage.ChangeViewOnCarousel(Defaults.CarouselOutfitBuilder);
        }

        [When(@"I click on Outfit Builder button")]
        public void WhenIClickOnOutfitBuilderButton()
        {
            var outfitBuilderPage = MainPage.ClickOnGetStartedButton();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region OutfitBuilderGroomGoesFree

        [Given(@"I have proceed to Groom Goes Free menu on Carousel")]
        public void GivenIHaveProceedToGroomGoesFreeMenuOnCarousel()
        {
            MainPage.ChangeViewOnCarousel(Defaults.CarouselGroomGoesFree);
        }

        [Given(@"I click on Find out more button")]
        public void GivenIClickOnFindOutMoreButton()
        {
            var groomGoesFreePage = MainPage.ClickOnFindOutMoreGroomButton();

            ScenarioContext.Current.Add("GroomGoesFreePage", groomGoesFreePage);
        }

        [When(@"I click on Get Started button")]
        public void WhenIClickOnGetStartedButton()
        {
            var groomGoesFreePage = new GroomGoesFreePage();
            ScenarioContext.Current.TryGetValue("GroomGoesFreePage", out groomGoesFreePage);

            var outfitBuilderPage = groomGoesFreePage.ClickOnGetStartedButton();
            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region OutfitBuilderTryOn

        [Given(@"I have proceed to Free Try On menu on Carousel")]
        public void GivenIHaveProceedToFreeTryOnMenuOnCarousel()
        {
            MainPage.ChangeViewOnCarousel(Defaults.CarouselFreeTryOn);
        }

        [Given(@"I click on Find out more button on Try On Carousel")]
        public void GivenIClickOnFindOutMoreButtonOnTryOnCarousel()
        {
            var bookTryOnPage = MainPage.ClickOnFindOutMoreTryOnButton();

            ScenarioContext.Current.Add("BookTryOnPage", bookTryOnPage);
        }

        [When(@"I click on Get Started Try On button")]
        public void WhenIClickOnGetStartedTryOnButton()
        {
            var bookTryOnPage = new BookTryOnPage();
            ScenarioContext.Current.TryGetValue("BookTryOnPage", out bookTryOnPage);

            var outfitBuilderPage = bookTryOnPage.ClickOnGetStartedTryOnButton();
            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region OutfitBuilderTryOnOtherWay

        [When(@"I click on second Get Started Try On button")]
        public void WhenIClickOnSecondGetStartedTryOnButton()
        {
            var bookTryOnPage = new BookTryOnPage();
            ScenarioContext.Current.TryGetValue("BookTryOnPage", out bookTryOnPage);

            var outfitBuilderPage = bookTryOnPage.ClickOnGetStartedOutfitBuilderButton();
            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion
    }
}
