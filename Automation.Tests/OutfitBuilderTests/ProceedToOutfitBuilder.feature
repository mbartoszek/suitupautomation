﻿Feature: ProceedToOutfitBuilder
	As a user
	I want to click on Outfit Builder link
	In order to start designing my own suit

@OutfitBuilderLink
Scenario: Proceed to Outfit Builder via link
	When I click on Outfit Builder link
	Then I should be redirected to Outfit Builder page

@OutfitBuilderBurger
Scenario: Proceed to Outfit Builder via burger
	Given I have opened the navigation menu
	When I click on Outfit Builder link on navigation menu
	Then I should be redirected to Outfit Builder page

@OutfitBuilderCarousel
Scenario: Proceed to Outfit Builder via button Get Started on Carousel
	Given I have proceed to Outfit Builder menu on Carousel
	When I click on Outfit Builder button
	Then I should be redirected to Outfit Builder page

@OutfitBuilderGroomGoesFree
Scenario: Proceed to Outfit Builder via button Get Started on Groom Goes Free Page
	Given I have proceed to Groom Goes Free menu on Carousel
	Given I click on Find out more button
	When I click on Get Started button
	Then I should be redirected to Outfit Builder page

@OutfitBuilderTryOn
Scenario: Proceed to Outfit Builder via button Get Started on Free Try On Page
	Given I have proceed to Free Try On menu on Carousel
	Given I click on Find out more button on Try On Carousel
	When I click on Get Started Try On button
	Then I should be redirected to Outfit Builder page

@OutfitBuilderTryOnOtherWay
Scenario: Proceed to Outfit Builder via second button Get Started on Free Try On Page
	Given I have proceed to Free Try On menu on Carousel
	Given I click on Find out more button on Try On Carousel
	When I click on second Get Started Try On button
	Then I should be redirected to Outfit Builder page


