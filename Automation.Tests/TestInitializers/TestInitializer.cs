﻿using Automation.Configuration;
using Automation.Core;
using Automation.Core.Pages;
using NLog;
using TechTalk.SpecFlow;

namespace Automation.Tests.TestInitializers
{
    [Binding]
    public class TestInitializer
    {
        public static MainPage MainPage;
        public static InspireMePage InspireMePage;
        public static AccountPage AccountPage;

        [BeforeScenario("OutfitBuilderLink", "OutfitBuilderBurger", "OutfitBuilderCarousel","OutfitBuilderGroomGoesFree",
            "OutfitBuilderTryOn", "OutfitBuilderTryOnOtherWay", "OutfitBuilderButtonOfTheShadesOfGrey",
            "OutfitBuilderLinkOfTheShadesOfGrey",
            "OutfitBuilderButtonOfTheColourMeBold", "OutfitBuilderLinkOfTheColourMeBold",
            "OutfitBuilderLinkOfTheBlackTailcoat", "OutfitBuilderButtonOfTheBlackTailcoat",
            "OutfitBuilderLinkOfTheGreySilkTailcoat", "OutfitBuilderButtonOfTheGreySilkTailcoat",
            "OutfitBuilderLinkOfTheBlackSlimFit",
            "OutfitBuilderButtonOfTheBlackSlimFit", "OutfitBuilderLinkOfTheNavyPrinceEdward",
            "OutfitBuilderButtonOfTheNavyPrinceEdward",
            "TitleOfTheBuildPage", "TitleOfTheInspireMePage", "TitleOfTheGroomGoesFreePage", "TitleOfTheBookTryOnPage",
            "TitleOfTheHowItWorksPage", "TitleOfTheAccountPage",
            "TitleOfTheSubscribePage", "TitleOfTheTermsConditionsPage", "TitleOfTheCookiesPrivacyPage",
            "TitleOfTheHelpFAQsPage", "TitleOfThePriceListPage", "TitleOfTheXedoPage", "FilterShirts", "FilterAccessories",
            "ValidLogin", "InvalidUsername", "InvalidPassword", "InvalidCredentials")]
        public static void StartApplicationWithoutLogin()
        {
            Config.Load();

            LogManager.GetCurrentClassLogger()
                      .Info(@"Test: '{0}', scenario: '{1}' has been started.",
                            ScenarioContext.Current.ScenarioInfo.Title,
                            ScenarioContext.Current.ScenarioInfo.Tags[0]);

            WebBrowser.Open(Config.AppSettings.Browser);
            WebBrowser.WebDriver.Manage().Window.Maximize();

            MainPage = Page.Open<MainPage>();
        }

        [AfterScenario("OutfitBuilderLink", "OutfitBuilderBurger", "OutfitBuilderCarousel", "OutfitBuilderGroomGoesFree",
            "OutfitBuilderTryOn", "OutfitBuilderTryOnOtherWay", "OutfitBuilderButtonOfTheShadesOfGrey",
            "OutfitBuilderLinkOfTheShadesOfGrey",
            "OutfitBuilderButtonOfTheColourMeBold", "OutfitBuilderLinkOfTheBlackTailcoat",
            "OutfitBuilderLinkOfTheColourMeBold", "OutfitBuilderButtonOfTheBlackTailcoat",
            "OutfitBuilderLinkOfTheGreySilkTailcoat",
            "OutfitBuilderButtonOfTheGreySilkTailcoat", "OutfitBuilderLinkOfTheBlackSlimFit",
            "OutfitBuilderButtonOfTheBlackSlimFit",
            "OutfitBuilderLinkOfTheNavyPrinceEdward", "OutfitBuilderButtonOfTheNavyPrinceEdward", "TitleOfTheBuildPage",
            "TitleOfTheInspireMePage", "TitleOfTheGroomGoesFreePage", "TitleOfTheBookTryOnPage",
            "TitleOfTheHowItWorksPage", "TitleOfTheAccountPage",
            "TitleOfTheSubscribePage", "TitleOfTheTermsConditionsPage", "TitleOfTheCookiesPrivacyPage",
            "TitleOfTheHelpFAQsPage", "TitleOfThePriceListPage", "TitleOfTheXedoPage", "FilterShirts", "FilterAccessories",
            "ValidLogin", "InvalidUsername", "InvalidPassword", "InvalidCredentials")]
        public static void TestScenarioCleanup()
        {
            WebBrowser.Close();

            LogManager.GetCurrentClassLogger()
                      .Info(@"Test: '{0}', scenario: '{1}' has been finished.",
                            ScenarioContext.Current.ScenarioInfo.Title,
                            ScenarioContext.Current.ScenarioInfo.Tags[0]);
        }

        public string CurrentTitleOfThePage()
        {
            return WebBrowser.WebDriver.Title;
        }

        public void CurrentTab()
        {
            WebBrowser.WebDriver.SwitchToTab(1);
        }
    }
}
