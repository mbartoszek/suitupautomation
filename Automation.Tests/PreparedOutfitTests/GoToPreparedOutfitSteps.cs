﻿using Automation.Core.Pages;
using Automation.Tests.TestInitializers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Tests.PreparedOutfitTests
{
    [Binding]
    public class GoToPreparedOutfitSteps : TestInitializer
    {
        #region @OutfitBuilderButtonOfTheShadesOfGrey

        [When(@"I click on View Outfits button of the fifty Shades of Grey")]
        public void WhenIClickOnViewOutfitsButtonOfTheFiftyShadesOfGrey()
        {
            var outfitBuilderPage = MainPage.ClickOnViewOutFitsButtonOfFiftyShadesOfGrey();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"I see selected grey colour of the fifty Shades of Grey")]
        public void ThenISeeSelectedGreyColourOfTheFiftyShadesOfGrey()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckSelectedGreySuitsForFifityShadesOfGrey(), "The Grey suits is not automatically seleted on Outfit Builder Page");
        }

        #endregion

        #region OutfitBuilderLinkOfTheShadesOfGrey

        [When(@"I click on the fifty Shades of Grey link")]
        public void WhenIClickOnTheFiftyShadesOfGreyLink()
        {
            var outfitBuilderPage = MainPage.ClickOnTheFiftyShadesOfGreyLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region @OutfitBuildeButtonOfTheColourMeBold

        [When(@"I click on View Outfits button of the Colour Me Bold")]
        public void WhenIClickOnViewOutfitsButtonOfTheColourMeBold()
        {
            var outfitBuilderPage = MainPage.ClickOnViewOutfitsButtonOfColourMeBold();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"I see selected clothes of the Colour Me Bold")]
        public void ThenISeeSelectedClothesOfTheColourMeBold()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckSelectedRucheForColourMeBold(), "A ruche is not automatically seleted on Outfit Builder Page");
        }

        #endregion

        #region OutfitBuilderLinkOfTheColourMeBold

        [When(@"I click on the Colour Me Bold link")]
        public void WhenIClickOnTheColourMeBoldLink()
        {
            var outfitBuilderPage = MainPage.ClickOnViewTheColourMeBoldLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion
        
        #region OutfitBuilderLinkOfTheBlackTailcoat

        [When(@"I click on Black Tailcoat link")]
        public void WhenIClickOnBlackTailcoatLink()
        {
            var outfitBuilderPage = MainPage.ClickOnBlackTailcoatLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"ensure that the Black Tailcoat image is displayed properly")]
        public void ThenEnsureThatTheBlackTailcoatImageIsDisplayedProperly()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckImageOfTheBlackTailcoat(), "The image of the Black Tailcoat is not displayed properly on Outfit Builder Page");
        }
        
        #endregion

        #region OutfitBuilderButtonOfTheBlackTailcoat

        [When(@"I click on View Outfit button of the Black Tailcoat")]
        public void WhenIClickOnViewOutfitButtonOfTheBlackTailcoat()
        {
            var outfitBuilderPage = MainPage.ClickOnViewOutfitButtonOfTheBlackTailcoat();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

         #endregion

        #region OutfitBuilderLinkOfTheGreySilkTailcoat

        [When(@"I click on Grey Silk Tailcoat link")]
        public void WhenIClickOnGreySilkTailcoatLink()
        {
            var outfitBuilderPage = MainPage.ClickOnGreySilkTailcoatLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"ensure that the Grey Silk Tailcoat image is displayed properly")]
        public void ThenEnsureThatTheGreySilkTailcoatImageIsDisplayedProperly()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckImageOfTheGreySilkTailCoat(), "The image of the Grey Silk Tailcoat is not displayed properly on Outfit Builder Page");
        }

        #endregion

        #region OutfitBuilderButtonOfTheGreySilkTailcoat

        [When(@"I click on View Outfit button of the Grey Silk Tailcoat")]
        public void WhenIClickOnViewOutfitButtonOfTheGreySilkTailcoat()
        {
            var outfitBuilderPage = MainPage.ClickOnViewOutfitButtonOfTheGreySilkTailcoat();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region OutfitBuilderLinkOfTheBlackSlimFit

        [When(@"I click on Black Slim Fit  link")]
        public void WhenIClickOnBlackSlimFitLink()
        {
            var outfitBuilderPage = MainPage.ClickOnBlackSlimFitLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"ensure that the Black Slim Fit image is displayed properly")]
        public void ThenEnsureThatTheBlackSlimFitTImageIsDisplayedProperly()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckImageOfTheBlackSlimFit(), "The image of the Grey Silk Tailcoat is not displayed properly on Outfit Builder Page");
        }

        #endregion

        #region OutfitBuilderButtonOfTheBlackSlimFit

        [When(@"I click on View Outfit button of the Black Slim Fit")]
        public void WhenIClickOnViewOutfitButtonOfTheBlackSlimFit()
        {
            var outfitBuilderPage = MainPage.ClickOnViewOutfitButtonOfTheBlackSlimFit();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion

        #region OutfitBuilderLinkOfTheNavyPrinceEdward

        [When(@"I click on Navy Prince Edward link")]
        public void WhenIClickOnNavyPrinceEdwardLink()
        {
            var outfitBuilderPage = MainPage.ClickOnNavyPrinceEdnwardLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [Then(@"ensure that the Navy Prince Edward image is displayed properly")]
        public void ThenEnsureThatTheNavyPrinceEdwardImageIsDisplayedProperly()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckImageOfTheNavyPrinceEdward(), "The image of the Grey Silk Tailcoat is not displayed properly on Outfit Builder Page");
        }

        #endregion

        #region OutfitBuilderLinkOfTheNavyPrinceEdward

        [When(@"I click on View Outfit button of the Navy Prince Edward")]
        public void WhenIClickOnViewOutfitButtonOfTheNavyPrinceEdward()
        {
            var outfitBuilderPage = MainPage.ClickOnViewOutfitButtonOfTheNavyPrinceEdward();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        #endregion
    }
}
