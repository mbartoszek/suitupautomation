﻿Feature: GoToPreparedOutfit
	As a user
	I want to click on View Outfit link or the desired button
	In order to see prepared offer of the outfits

#This Season's Trends
@OutfitBuilderButtonOfTheShadesOfGrey
Scenario: View Outfits of the fifty Shades of Grey via button
	When I click on View Outfits button of the fifty Shades of Grey
	Then I should be redirected to Outfit Builder page
	And I see selected grey colour of the fifty Shades of Grey

@OutfitBuilderLinkOfTheShadesOfGrey
Scenario: View Outfits of the fifty Shades of Grey via link
	When I click on the fifty Shades of Grey link
	Then I should be redirected to Outfit Builder page
	And I see selected grey colour of the fifty Shades of Grey

@OutfitBuilderButtonOfTheColourMeBold
Scenario: View Outfits of the Colour Me Bold via button 
	When I click on View Outfits button of the Colour Me Bold
	Then I should be redirected to Outfit Builder page
	Then I see selected clothes of the Colour Me Bold

@OutfitBuilderLinkOfTheColourMeBold
Scenario: View Outfits of the Colour Me Bold via link
	When I click on the Colour Me Bold link
	Then I should be redirected to Outfit Builder page
	Then I see selected clothes of the Colour Me Bold

#Recommended Suits
@OutfitBuilderLinkOfTheBlackTailcoat
Scenario: View Outfit of the Black Tailcoat via link
	When I click on Black Tailcoat link
	Then I should be redirected to Outfit Builder page
	And ensure that the Black Tailcoat image is displayed properly

@OutfitBuilderButtonOfTheBlackTailcoat
Scenario:  View Outfit of the Black Tailcoat via button
	When I click on View Outfit button of the Black Tailcoat
	Then I should be redirected to Outfit Builder page
	And ensure that the Black Tailcoat image is displayed properly

@OutfitBuilderLinkOfTheGreySilkTailcoat
Scenario: View Outfit of the Grey Silk Tailcoat via link
	When I click on Grey Silk Tailcoat link
	Then I should be redirected to Outfit Builder page
	And ensure that the Grey Silk Tailcoat image is displayed properly

@OutfitBuilderButtonOfTheGreySilkTailcoat
Scenario:  View Outfit of the Grey Silk Tailcoat via button
	When I click on View Outfit button of the Grey Silk Tailcoat
	Then I should be redirected to Outfit Builder page
	And ensure that the Grey Silk Tailcoat image is displayed properly

@OutfitBuilderLinkOfTheBlackSlimFit 
Scenario: View Outfit of the Black Slim Fit via link
	When I click on Black Slim Fit  link
	Then I should be redirected to Outfit Builder page
	And ensure that the Black Slim Fit image is displayed properly

@OutfitBuilderButtonOfTheBlackSlimFit 
Scenario:  View Outfit of the Black Slim Fit via button
	When I click on View Outfit button of the Black Slim Fit 
	Then I should be redirected to Outfit Builder page
	And ensure that the Black Slim Fit image is displayed properly

@OutfitBuilderLinkOfTheNavyPrinceEdward
Scenario: View Outfit of the Navy Prince Edward via link
	When I click on Navy Prince Edward link
	Then I should be redirected to Outfit Builder page
	And ensure that the Navy Prince Edward image is displayed properly

@OutfitBuilderButtonOfTheNavyPrinceEdward
Scenario:  View Outfit of the Navy Prince Edward via button
	When I click on View Outfit button of the Navy Prince Edward 
	Then I should be redirected to Outfit Builder page
	And ensure that the Navy Prince Edward image is displayed properly
