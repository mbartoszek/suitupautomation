﻿using System;
using Automation.Configuration;
using Automation.Core.Pages;
using Automation.Tests.TestInitializers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Tests.InspireMeTests
{
    [Binding]
    public class GoToInspireMeSteps : TestInitializer
    {
        private string invalidImage = "The image has not been loaded correctly";

        [When(@"I click on Inspire Me button")]
        public void WhenIClickOnInspireMeButton()
        {
            var inspireMePage = MainPage.ClickOnInspireMeLink();

            ScenarioContext.Current.Add("InspireMePage", inspireMePage);
        }
        
        [When(@"I select the ""(.*)""")]
        public void WhenISelectThe(string numberOfThePhoto)
        {
            InspireMePage inspireMePage = new InspireMePage();
            ScenarioContext.Current.TryGetValue("InspireMePage", out inspireMePage);

            inspireMePage.ChangeViewOnCarousel(numberOfThePhoto);

            ScenarioContext.Current.Add("InspireMePage1", inspireMePage);
        }

        [When(@"I press the View in Outfit Builder button of the ""(.*)""")]
        public void WhenIPressTheViewInOutfitBuilderButton(string numberOfThePhoto)
        {
            InspireMePage inspireMePage = new InspireMePage();
            ScenarioContext.Current.TryGetValue("InspireMePage1", out inspireMePage);

            var outfitBuilderPage = inspireMePage.ClickOnViewInOutfitBuilderButton(numberOfThePhoto);

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }
        
        [Then(@"ensure that the image of the ""(.*)"" is displayed properly")]
        public void ThenEnsureThatTheImageOfTheIsDisplayedProperly(string numberOfThePhoto)
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            switch (numberOfThePhoto)
            {
                case Defaults.InspireMeTheFirstOutfit:
                    Assert.IsTrue(outfitBuilderPage.CheckImageOfTheFirstOutfit(), invalidImage);
                    break;
                case Defaults.InspireMeTheSecondtOutfit:
                    Assert.IsTrue(outfitBuilderPage.CheckImageOfTheSecondOutfit(), invalidImage);
                    break;
                case Defaults.InspireMeTheThirdOutfit:
                    Assert.IsTrue(outfitBuilderPage.CheckImageOfTheThirdOutfit(), invalidImage);
                    break;
                case Defaults.InspireMeTheFourthOutfit:
                    Assert.IsTrue(outfitBuilderPage.CheckImageOfTheFourthOutfit(), invalidImage);
                    break;
                case Defaults.InspireMeTheFifthOutfit:
                    Assert.IsTrue(outfitBuilderPage.CheckImageOfTheFifthOutfit(), invalidImage);
                    break;
                case Defaults.InspireMeTheSixthOutfit:
                    Assert.IsTrue(outfitBuilderPage.CheckImageOfTheSixthOutfit(), invalidImage);
                    break;
            }

        }
    }
}
