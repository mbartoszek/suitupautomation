﻿Feature: GoToInspireMe
	As a user
	I want to click on Inspire Me button
	In order to see prepared offer of the outfits

@OutfitBuilderButtonOfTheShadesOfGrey
Scenario Outline: View Outfits of the Inspire Me via button
	When I click on Inspire Me button
	And I select the "<NumberOfThePhoto>"
	And I press the View in Outfit Builder button of the "<NumberOfThePhoto>"
	Then ensure that the image of the "<NumberOfThePhoto>" is displayed properly
Examples: 
| NumberOfThePhoto |
| Outfit No. 1     |
| Outfit No. 2     |
| Outfit No. 3     |
| Outfit No. 4     |
| Outfit No. 5     |
| Outfit No. 6     |


