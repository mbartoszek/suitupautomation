﻿using Automation.Configuration;
using Automation.Core.Pages;
using Automation.Tests.TestInitializers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Tests.FilterOutfitTests
{
    public class FilterOutfitSteps : TestInitializer
    {
        #region FilterSuits

        [Given(@"I click on Outfit Builder feature area")]
        public void GivenIClickOnOutfitBuilderFeatureArea()
        {
            var outfitBuilderPage = MainPage.ClickOnOutfitBuilderLink();

            ScenarioContext.Current.Add("OutfitBuilderPage", outfitBuilderPage);
        }

        [When(@"I set ""(.*)"" as a colour filter in suits category")]
        public void WhenISetAsAColourFilterInSuitsCategory(string colour)
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            outfitBuilderPage.ClearFilter(Defaults.OutfitCategorySuits);
            outfitBuilderPage.FilterBySuits(colour, string.Empty);

            ScenarioContext.Current.Add("SuitColour", colour);
        }

        [When(@"I set ""(.*)"" as a style filter in suits category")]
        public void WhenISetAsAStyleFilterInSuitsCategory(string style)
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            outfitBuilderPage.FilterBySuits(string.Empty, style);

            ScenarioContext.Current.Add("SuitStyle", style);
        }

        [Then(@"Suits should be filtered according to rules")]
        public void ThenSuitsShouldBeFilteredAccordingToRules()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            var suitColour = string.Empty;
            var suitStyle = string.Empty;
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);
            ScenarioContext.Current.TryGetValue("SuitColour", out suitColour);
            ScenarioContext.Current.TryGetValue("SuitStyle", out suitStyle);

            Assert.IsTrue(outfitBuilderPage.CheckIfSuitsHaveBeenFiltered(suitColour, suitStyle), string.Format("Suits have not been filtered properly for: {0} colour and {1} style", suitColour, suitStyle));
        }

        #endregion

        #region FilterWaistcoats

        [When(@"I set ""(.*)"" as a colour filter in waistcoats category")]
        public void WhenISetAsAColourFilterInWaistcoatsCategory(string colour)
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            outfitBuilderPage.FilterByWaistcoats(colour);

            ScenarioContext.Current.Add("WaistcoatsColour", colour);
        }

        [Then(@"Waistcoats should be filtered according to rules")]
        public void ThenWaistcoatsShouldBeFilteredAccordingToRules()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            var waistcoatsColour = string.Empty;
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);
            ScenarioContext.Current.TryGetValue("WaistcoatsColour", out waistcoatsColour);

            Assert.IsTrue(outfitBuilderPage.CheckIfWaistcoatsHaveBeenFiltered(waistcoatsColour), string.Format("Waistcoats have not been filtered properly for: {0} colour", waistcoatsColour));
        }

        #endregion

        #region FilterNeckwear

        [When(@"I set ""(.*)"" as a colour filter in neckwear category")]
        public void WhenISetAsAColourFilterInNeckwearCategory(string colour)
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            outfitBuilderPage.FilterByNeckwear(colour);

            ScenarioContext.Current.Add("NeckwearColour", colour);
        }

        [Then(@"Neckwear should be filtered according to rules")]
        public void ThenNeckwearShouldBeFilteredAccordingToRules()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            var neckwearColour = string.Empty;
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);
            ScenarioContext.Current.TryGetValue("NeckwearColour", out neckwearColour);

            Assert.IsTrue(outfitBuilderPage.CheckIfNeckwearHaveBeenFiltered(neckwearColour), string.Format("Neckwear have not been filtered properly for: {0} colour", neckwearColour));
        }

        #endregion

        #region FilterShirts

        [When(@"I click on Shirts category")]
        public void WhenIClickOnShirtsCategory()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            outfitBuilderPage.FilterByShirts();
        }

        [Then(@"All Shirts should be listed")]
        public void ThenAllShirtsShouldBeListed()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckIfShirtsHaveBeenFiltered(), "Shirts have not been displayed properly");
        }

        #endregion

        #region FilterAccessories

        [When(@"I click on Accessories category")]
        public void WhenIClickOnAccessoriesCategory()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            outfitBuilderPage.FilterByAccessories();
        }

        [Then(@"All Accessories should be listed")]
        public void ThenAllAccessoriesShouldBeListed()
        {
            var outfitBuilderPage = new OutfitBuilderPage();
            ScenarioContext.Current.TryGetValue("OutfitBuilderPage", out outfitBuilderPage);

            Assert.IsTrue(outfitBuilderPage.CheckIfAccessoriesHaveBeenFiltered(), "Accessories have not been displayed properly");
        }

        #endregion
    }
}
