﻿Feature: FilterOutfit
	As a user
	I want to set filters on outfit
	In order to get filtered results

@FilterSuits
Scenario Outline: Filter Suits by setting desired colour and style
	Given I click on Outfit Builder feature area
	When I set "<Colour>" as a colour filter in suits category
	And I set "<Style>" as a style filter in suits category
	Then Suits should be filtered according to rules
	Examples:
	| Colour | Style                               |
	|        |                                     |
	|        | Tailcoat                            |
	|        | Short Jacket                        |
	|        | Prince Edward                       |
	|        | Tailcoat,Short Jacket               |
	|        | Tailcoat,Short Jacket,Prince Edward |
	| Grey   |                                     |
	| Grey   | Tailcoat                            |
	| Grey   | Short Jacket                        |
	| Grey   | Prince Edward                       |
	| Grey   | Tailcoat,Short Jacket               |
	| Grey   | Tailcoat,Short Jacket,Prince Edward |
	| Blue   |                                     |
	| Blue   | Tailcoat                            |
	| Blue   | Short Jacket                        |
	| Blue   | Prince Edward                       |
	| Blue   | Tailcoat,Short Jacket               |
	| Blue   | Tailcoat,Short Jacket,Prince Edward |
	| Black  |                                     |
	| Black  | Tailcoat                            |
	| Black  | Short Jacket                        |
	| Black  | Prince Edward                       |
	| Black  | Tailcoat,Short Jacket               |
	| Black  | Tailcoat,Short Jacket,Prince Edward |

@FilterWaistcoats
Scenario Outline: Filter Waistcoats by setting desired colour
	Given I click on Outfit Builder feature area
	When I set "<Colour>" as a colour filter in waistcoats category
	Then Waistcoats should be filtered according to rules
	Examples:
	| Colour |
	| Grey   |
	| Pink   |
	| Purple |
	| Blue   |
	| Teal   |
	| Green  |
	| Yellow |
	| Cream  |
	| Orange |
	| Red    |
	| Brown  |
	| Black  |
	|        |

@FilterNeckwear
Scenario Outline: Filter Neckwear by setting desired colour
	Given I click on Outfit Builder feature area
	When I set "<Colour>" as a colour filter in neckwear category
	Then Neckwear should be filtered according to rules
	Examples:
	| Colour |
	| Grey   |
	| Pink   |
	| Purple |
	| Blue   |
	| Teal   |
	| Green  |
	| Yellow |
	| Cream  |
	| Orange |
	| Red    |
	| Brown  |
	| Black  |
	| White  |
	|        |

@FilterShirts
Scenario: Filter Shirts by clicking on shirts category
	Given I click on Outfit Builder feature area
	When I click on Shirts category
	Then All Shirts should be listed

@FilterAccessories
Scenario: Filter Accessories by clicking on accessories category
	Given I click on Outfit Builder feature area
	When I click on Accessories category
	Then All Accessories should be listed