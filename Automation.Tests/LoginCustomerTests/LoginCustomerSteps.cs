﻿using Automation.Core.Pages;
using Automation.Tests.TestInitializers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Automation.Tests.LoginCustomerTests
{
    [Binding]
    public class GoToPreparedOutfitSteps : TestInitializer
    {
        [Given(@"I click on Sign in link")]
        public void GivenIClickOnSignInLink()
        {
            var mainPage = MainPage.ClickOnSignInLink();
            ScenarioContext.Current.Add("MainPage", mainPage);
        }

        [Given(@"I enter ""(.*)"" as userneme")]
        public void GivenIEnterAsUserneme(string username)
        {
            var mainPage = new MainPage();
            ScenarioContext.Current.TryGetValue("MainPage", out mainPage);

            var mainPageUsername = MainPage.EnterUsername(username);
            ScenarioContext.Current.Add("MainPageWithUsername", mainPageUsername);
        }

        [Given(@"I enter ""(.*)"" as password")]
        public void GivenIEnterAsPassword(string password)
        {
            var mainPageUsername = new MainPage();
            ScenarioContext.Current.TryGetValue("MainPageWithUsername", out mainPageUsername);

            var mainPagePassword = MainPage.EnterPassword(password);
            ScenarioContext.Current.Add("MainPageWithPassword", mainPagePassword);
        }

        [When(@"I click on Login button")]
        public void WhenIClickOnLoginButton()
        {
            var mainPagePassword = new MainPage();
            ScenarioContext.Current.TryGetValue("MainPageWithPassword", out mainPagePassword);

            var ordersPage = MainPage.ClickOnLoginInButton();
            ScenarioContext.Current.Add("OrdersPage", ordersPage);
        }

        [Then(@"I ensrue that I have been logged in to the application")]
        public void ThenIEnsrueThatIHaveBeenLoggedInToTheApplication()
        {
            Assert.IsTrue(MainPage.CheckIfUserIsLogged(), "A user should be logged in to the web application");
        }
        
        [Then(@"I ensrue that username is incorrect")]
        public void ThenIEnsrueThatUsernameIsIncorrect()
        {
            Assert.IsTrue(MainPage.CheckUsername(), "Information about invalid username should be displayed");
        }

        [Then(@"I ensrue that password is incorrect")]
        public void ThenIEnsrueThatPasswordIsIncorrect()
        {
            Assert.IsTrue(MainPage.CheckPassword(), "Information about invalid password should be displayed");
        }

        [Then(@"I ensrue that credentials are incorrect")]
        public void ThenIEnsrueThatCredentialsAreIncorrect()
        {
            Assert.IsTrue(MainPage.CheckPassword() && MainPage.CheckUsername(), "Information about invalid credentials should be displayed");
        }

    }
}
