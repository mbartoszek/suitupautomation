﻿Feature: LoginCustomer
	As a user
	I want to log in to the web application
	In order to have an access

@ValidLogin
Scenario Outline: Valid customer login
	Given I click on Sign in link
	And I enter "<CustomerEmail>" as userneme
	And I enter "<CustomerPassword>" as password
	When I click on Login button
	Then I ensrue that I have been logged in to the application
		Examples: 
	| CustomerEmail                 | CustomerPassword |
	| testaccount1@xedosoftware.com | a1s2d3f4g5@      |

@InvalidUsername
Scenario Outline: Invalid customer's username
	Given I click on Sign in link
	And I enter "<CustomerEmail>" as userneme
	And I enter "<CustomerPassword>" as password
	When I click on Login button
	Then I ensrue that username is incorrect
	Examples: 
	| CustomerEmail | CustomerPassword |
	| sasa          | a1s2d3f4g5@      |
	| testaccount1  | a1s2d3f4g5@      |
	|               | a1s2d3f4g5@      |

@InvalidPassword
Scenario Outline: Invalid customer's password
	Given I click on Sign in link
	And I enter "<CustomerEmail>" as userneme
	And I enter "<CustomerPassword>" as password
	When I click on Login button
	Then I ensrue that password is incorrect
	Examples: 
	| CustomerEmail                 | CustomerPassword  |
	| testaccount1@xedosoftware.com | IncorrectPassword |
	| testaccount1@xedosoftware.com |                   |


@InvalidCredentials
Scenario Outline: Invalid customer's credentials
	Given I click on Sign in link
	And I enter "<CustomerEmail>" as userneme
	And I enter "<CustomerPassword>" as password
	When I click on Login button
	Then I ensrue that credentials are incorrect
	Examples: 
	| CustomerEmail | CustomerPassword |
	| Incorect      | incorect         |
	|               |                  |