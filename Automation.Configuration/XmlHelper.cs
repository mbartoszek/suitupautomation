﻿using Automation.Configuration.Exceptions;
using NLog;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Automation.Configuration
{   
    /// <summary>
    /// Methods to help with operation on Xml files.
    /// </summary>
    public static class XmlHelper
    {
        #region Methods

        /// <summary>
        /// Loads the XML from file and validate schema.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <returns>Validated XMLDocument object.</returns>
        private static XmlDocument LoadXmlFromFile(string xmlFilePath, string xmlSchemaPath)
        {
            var settings = new XmlReaderSettings();
            if (!string.IsNullOrEmpty(xmlSchemaPath))
            {
                settings.Schemas.Add(null, xmlSchemaPath);
                settings.ValidationType = ValidationType.Schema;    
            }

            var reader = XmlReader.Create(xmlFilePath, settings);
            var document = new XmlDocument();
            document.Load(reader);

            return document;
        }

        /// <summary>
        /// Open XML document and return all root childs nodes.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <returns>
        /// List of root child nodes.
        /// </returns>
        /// <exception cref="XmlValidationException">Thrown when structure in XML file is wrong.</exception>
        public static XPathNodeIterator LoadDocumentNodes(string xmlFilePath, string xmlSchemaPath)
        {
            var documentNavigator = LoadDocument(xmlFilePath, xmlSchemaPath).CreateNavigator();
            if (documentNavigator == null)
                throw new XmlValidationException(string.Format("XML document '{0}' was not loaded correctly.", xmlFilePath));

            return documentNavigator.SelectChildren(XPathNodeType.Element);
        }

        /// <summary>
        /// Load XML document and return it.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <returns>
        /// Opened XML document.
        /// </returns>
        public static IXPathNavigable LoadDocument(string xmlFilePath, string xmlSchemaPath)
        {
            LogManager.GetCurrentClassLogger().Info("Loading Xml document from path: '{0}'.", xmlFilePath);
            return LoadXmlFromFile(xmlFilePath, xmlSchemaPath);
        }

        /// <summary>
        /// Serializes object to XML.
        /// </summary>
        /// <typeparam name="T">Type of object to serialize.</typeparam>
        /// <param name="serializedObject">The object to serialize.</param>
        /// <param name="fileName">Name of the file.</param>
        public static void SerializeToXml<T>(T serializedObject, string fileName)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                serializer.Serialize(fileStream, serializedObject);
        }

        /// <summary>
        /// Deserializes from XML file to object.
        /// </summary>
        /// <typeparam name="T">Type of element to deserialize.</typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Object deserialized from XML file.</returns>
        public static T DeserializeFromXml<T>(string fileName)
        {
            T result;
            var ser = new XmlSerializer(typeof(T));
            using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                result = (T)ser.Deserialize(fileStream);

            return result;
        }

        /// <summary>
        /// Deserializes from XML document part to object.
        /// </summary>
        /// <typeparam name="T">Type of element to deserialize.</typeparam>
        /// <param name="xmlPart">The part of XML document.</param>
        /// <returns>Object deserialized from xml.</returns>
        public static T DeserializeFromXml<T>(IXPathNavigable xmlPart)
        {
            var ser = new XmlSerializer(typeof(T));
            var result = (T)ser.Deserialize(XmlReader.Create(new StringReader(xmlPart.CreateNavigator().OuterXml)));
            return result;
        }

        /// <summary>
        /// Gets the XML document part.
        /// </summary>
        /// <param name="elementType">Type of the element.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <returns>
        /// Part of xml file with element od type elementType and name elementName.
        /// </returns>
        public static IXPathNavigable GetXmlDocumentPart(string elementType, string elementName, string xmlFilePath, string xmlSchemaPath)
        {
            var resultXml = new XmlDocument();
            var sourceXml = LoadXmlFromFile(xmlFilePath, xmlSchemaPath);
            resultXml.AppendChild(resultXml.ImportNode(sourceXml.SelectSingleNode(string.Format("//{0}[@name=\"{1}\"]", elementType, elementName)), true));          
            return resultXml;
        }

        /// <summary>
        /// Search of elements in config file by Xpath.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlFileSchema">The XML file schema.</param>
        /// <param name="xpath">Xpath that indicate elements in config.</param>
        /// <returns>
        /// List of elements that has been found.
        /// </returns>
        /// <exception cref="XmlValidationException">Thrown when xml file was not correctly loaded.</exception>
        public static XPathNodeIterator GetNodeListByXpath(string xmlFilePath, string xmlFileSchema, string xpath)
        {
            var list = LoadDocument(xmlFilePath, xmlFileSchema).CreateNavigator();

            if (list == null)
                throw new XmlValidationException(string.Format("XML file '{0}' was not loaded correctly.", xmlFilePath));

            return list.Select(xpath);
        }

        /// <summary>
        /// Search of xml node element by name.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <param name="elementType">Type of element to search for it.</param>
        /// <param name="elementName">Name of element to search.</param>
        /// <returns>
        /// Node element that has been found.
        /// </returns>
        /// <exception cref="WrongConfigFileStructException">
        /// Thrown when structure of config file is incorrect.
        /// </exception>
        public static XPathNavigator GetElementByName(string xmlFilePath, string xmlSchemaPath, string elementType, string elementName)
        {
            var elementList = GetNodeListByXpath(xmlFilePath, xmlSchemaPath, string.Format("//{0}[@name='{1}']", elementType, elementName));

            if (elementList.Count > 1)
                throw new WrongConfigFileStructException(string.Format("There are more than one path definition for item with name {0}.", elementName));

            if (elementList.Current == null)
                throw new WrongConfigFileStructException(string.Format("Searched element with name {0} does not exist in config file.", elementName));
            elementList.MoveNext();

            return elementList.Current;
        }

        /// <summary>
        /// Search of element value by it's name.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <param name="elementType">Type of element to search for it.</param>
        /// <param name="elementName">Name of element to search.</param>
        /// <returns>
        /// Element value that has been found.
        /// </returns>
        public static string GetElementContent(string xmlFilePath, string xmlSchemaPath, string elementType, string elementName)
        {
            return GetElementByAttribute(xmlFilePath, xmlSchemaPath, elementType, elementName, "content");
        }

        /// <summary>
        /// Search of attribute value in specified element by names.
        /// </summary>
        /// <param name="xmlFilePath">The XML file path.</param>
        /// <param name="xmlSchemaPath">The XML schema path.</param>
        /// <param name="elementType">Type of element to search for it.</param>
        /// <param name="elementName">Name of element to search.</param>
        /// <param name="attributeName">Name of attribute to return.</param>
        /// <returns>
        /// Attribute value that has been found.
        /// </returns>
        /// <exception cref="WrongConfigFileStructException">Thrown when structure of config file is not correct.</exception>
        public static string GetElementByAttribute(string xmlFilePath, string xmlSchemaPath, string elementType, string elementName, string attributeName)
        {
            var attribute = GetElementByName(xmlFilePath, xmlSchemaPath, elementType, elementName).GetAttribute(attributeName, string.Empty);

            if (string.IsNullOrEmpty(attribute))
                throw new WrongConfigFileStructException(string.Format("There is no attribute {0} in element {1}.", attributeName, elementName));

            return attribute;
        }

        #endregion
    }
}
