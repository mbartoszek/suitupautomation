﻿using System;
using System.Runtime.Serialization;
using NLog;

namespace Automation.Configuration.Exceptions
{
    [Serializable]
    public class XmlValidationException : Exception
    {
        #region Constructors

        public XmlValidationException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlValidationException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public XmlValidationException(string message)
            : base(message)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlValidationException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public XmlValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
            LogManager.GetCurrentClassLogger().Error(message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlValidationException" /> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected XmlValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
    }
}
