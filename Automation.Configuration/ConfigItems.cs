﻿using System.Linq;
using System.Xml.XPath;

namespace Automation.Configuration
{
    public class ConfigItems
    {
        #region Constants

        private const string NameAttribute = "name";
        private const string ValueAttribute = "value";
        private readonly XPathNodeIterator _configItems;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ConfigItems class.
        /// </summary>
        /// <param name="configFilePath">The config file path.</param>
        /// <param name="configSchemaPath">The config schema path.</param>
        /// <param name="itemsXpath">The items xpath.</param>
        public ConfigItems(string configFilePath, string configSchemaPath, string itemsXpath)
        {
            _configItems = XmlHelper.GetNodeListByXpath(configFilePath, configSchemaPath, itemsXpath);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the browser
        /// </summary>
        public string Browser
        {
            get
            {
                return this["WebBrowser"];
            }
        }

        /// <summary>
        /// Gets the information about the WebDrivers folder.
        /// </summary>
        public string WebDriversFolder
        {
            get
            {
                return this["WebDriversFolder"];
            }
        }

        /// <summary>
        /// Gets the default url.
        /// </summary>
        public string DefaultUrl
        {
            get
            {
                return string.Format("{0}{1}", this["Scheme"], this["HostName"]);
            }
        }

        /// <summary>
        /// Gets the default timeout in milliseconds
        /// </summary>
        public int DefaultPageLoadTimeout
        {
            get
            {
                return int.Parse(this["DefaultPageLoadTimeout"]);
            }
        }

        public int DefaultFindElementTimeout
        {
            get
            {
                return int.Parse(this["DefaultFindElementTimeout"]);
            }
        }

        public int DefaultImplicitlyWait
        {
            get
            {
                return int.Parse(this["DefaultImplicitlyWait"]);
            }
        }

        /// <summary>
        /// Gets the host name
        /// </summary>
        public string HostName
        {
            get
            {
                return this["HostName"];
            }
        }

        /// <summary>
        /// Gets the scheme
        /// </summary>
        public string Scheme
        {
            get
            {
                return this["Scheme"];
            }
        }

        public bool KillBrowsers
        {
            get
            {
                return bool.Parse(this["KillBrowsers"]);
            }
        }

        public string Port
        {
            get
            {
                return this["Port"];
            }
        }

        public string MainPath
        {
            get
            {
                return this["MainPath"];
            }
        }

        public string DbConnectionTimeout
        {
            get
            {
                return this["DBConnectionTimeout"];
            }
        }

        public string DbCommandTimeout
        {
            get
            {
                return this["DBCommandTimeout"];
            }
        }

        public string DbUser
        {
            get
            {
                return this["DBUser"];
            }
        }

        public string DbPassword
        {
            get
            {
                return this["DBPassword"];
            }
        }

        public string DbServerName
        {
            get
            {
                return this["DBServerName"];
            }
        }

        public string DbName
        {
            get
            {
                return this["DBName"];
            }
        }

        #endregion

        #region Indexers

        /// <summary>
        /// Gets the config item with the specified name.
        /// </summary>
        /// <param name="name">The config item name.</param>
        /// <returns>Config value.</returns>
        public string this[string name]
        {
            get
            {
                return (from XPathNavigator configItem in _configItems where configItem.GetAttribute(NameAttribute, string.Empty).Equals(name) select configItem.GetAttribute(ValueAttribute, string.Empty)).FirstOrDefault();
            }
        }
        #endregion
    }
}
