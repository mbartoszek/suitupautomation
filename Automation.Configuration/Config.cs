﻿using System;
using System.Text;

namespace Automation.Configuration
{
    /// <summary>
    /// Functions to operate on test configuration files.
    /// </summary>
    public static class Config
    {
        #region Constants

        private const string ConfigPath = @"./ConfigFiles/TestsConfig.config";
        private const string ConfigSchemaPath = @"./ConfigFiles/TestsSchema.xsd";
        private const string ConfigKeysXpath = "//key";

        #endregion

        #region Properties

        /// <summary>
        /// Gets the test run settings.
        /// </summary>
        public static ConfigItems AppSettings { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the configuration.
        /// </summary>
        public static void Load()
        {
            AppSettings = new ConfigItems(ConfigPath, ConfigSchemaPath, ConfigKeysXpath);
        }

        /// <summary>
        /// Encodes the to64.
        /// </summary>
        /// <param name="toEncode">Text to encode.</param>
        /// <returns>Encoded data.</returns>
        public static string EncodeTo64(string toEncode)
        {
            var toEncodeAsBytes = Encoding.ASCII.GetBytes(toEncode);
            return Convert.ToBase64String(toEncodeAsBytes);
        }

        /// <summary>
        /// Decodes the from64.
        /// </summary>
        /// <param name="encodedData">The encoded data.</param>
        /// <returns>Decoded data.</returns>
        public static string DecodeFrom64(string encodedData)
        {
            var encodedDataAsBytes = Convert.FromBase64String(encodedData);
            return Encoding.ASCII.GetString(encodedDataAsBytes);
        }

        #endregion
    }
}
