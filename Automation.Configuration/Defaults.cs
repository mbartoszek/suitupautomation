﻿using System.Collections.Generic;

namespace Automation.Configuration
{
    public static class Defaults
    {
        public const string NavigationItemHome = "Home";
        public const string NavigationItemOutfitBuilder = "Outfit Builder";
        public const string NavigationItemInspireMe = "Inspire Me";
        public const string NavigationItemProfile = "Profile";

        public const string CarouselOutfitBuilder = "Outfit Builder";
        public const string CarouselInspireMe = "Inspire Me";
        public const string CarouselGroomGoesFree = "Groom Goes Free";
        public const string CarouselFreeTryOn = "Free Try On";

        public const string OutfitCategorySuits = "Suits";
        public const string OutfitCategoryWaistcoats = "Waistcoats";
        public const string OutfitCategoryNeckwear = "Neckwear";
        public const string OutfitCategoryShirts = "Shirts";
        public const string OutfitCategoryAccessories = "Accessories";

        public const string InspireMeTheFirstOutfit = "Outfit No. 1";
        public const string InspireMeTheSecondtOutfit = "Outfit No. 2";
        public const string InspireMeTheThirdOutfit = "Outfit No. 3";
        public const string InspireMeTheFourthOutfit = "Outfit No. 4";
        public const string InspireMeTheFifthOutfit = "Outfit No. 5";
        public const string InspireMeTheSixthOutfit = "Outfit No. 6";

        public static readonly Dictionary<string, string[]> StylesDictionary =
            new Dictionary<string, string[]>
            {
                {
                    "All", new []
                           {
                               "Tailcoat",
                               "Short Jacket",
                               "Short",
                               "Prince Edward"
                           }
                },
                {
                    "Tailcoat", new []
                                {
                                    "Tailcoat"
                                }
                },
                {
                    "Short Jacket", new []
                                    {
                                        "Short Jacket",
                                        "Short"
                                    }
                },
                {
                    "Prince Edward", new []
                                     {
                                         "Prince Edward"
                                     }
                }
            };

        public static readonly Dictionary<string, string[]> ShirtDictionary =
            new Dictionary<string, string[]>
            {
                {
                    "Type", new []
                                {
                                    "Classic Fit",
                                    "Slim Fit"
                                }
                },
                {
                    "Model", new []
                                {
                                    "Standard Collar Shirt",
                                    "Victorian Collar Shirt"
                                }
                }
            };

        public static readonly Dictionary<string, string[]> AccessoriesDictionary =
            new Dictionary<string, string[]>
            {
                {
                    "All", new []
                                {
                                    "Formal Shoes",
                                    "Cufflinks",
                                    "Black Top Hat",
                                    "Grey Top Hat"
                                }
                }
            };

        public static readonly Dictionary<string, string[]> ColoursDictionary = 
            new Dictionary<string, string[]>
            {
                {"All", new []
                            {
                                "Black",
                                "White",
                                "Blue",
                                "Navy",
                                "Cobalt",
                                "Mediterranean Blue",
                                "Marine Blue",
                                "Indigo",
                                "Once Upon A Time",
                                "Blue Box",
                                "Pool",
                                "Blue Jay",
                                "Grey",
                                "Slate Grey",
                                "Mid Grey",
                                "Dove Grey",
                                "Smoke",
                                "Charcoal",
                                "Moonlight Waltz",
                                "Brown",
                                "Stone",
                                "Rum Pink",
                                "Espresso",
                                "Chocolate",
                                "Mocha",
                                "Linen",
                                "Red",
                                "Claret",
                                "Berry",
                                "Mahogany",
                                "Cherry",
                                "Orange",
                                "Persimmon",
                                "Burnt Orange",
                                "Tangelo",
                                "Salmon",
                                "Cream",
                                "Ivory",
                                "Champagne",
                                "Yellow",
                                "Sunshine",
                                "Canary",
                                "Maize",
                                "Butter Cream",
                                "Green",
                                "Pistachio",
                                "Emerald",
                                "Shamrock",
                                "Forest",
                                "Clover",
                                "Sage",
                                "Sea Mist",
                                "Teal",
                                "Tealness",
                                "Jade",
                                "Beyond The Sea",
                                "Aqua",
                                "Purple",
                                "Eggplant",
                                "Viola",
                                "Victorian Lilac",
                                "Royal Bloom",
                                "Grape",
                                "Violet",
                                "Purple Storm",
                                "Peri",
                                "Lilac",
                                "Lavender",
                                "Pink",
                                "Pomegranate",
                                "Coral",
                                "Loves First Blush",
                                "Fuschia",
                                "Petunia",
                                "Tea Rose"
                            }},
                {"Black", new []
                            {
                                "Black"
                            }},
                {"White", new []
                            {
                                "White"
                            }},
                {"Blue", new []
                            {
                                "Blue",
                                "Navy",
                                "Cobalt",
                                "Mediterranean Blue",
                                "Marine Blue",
                                "Indigo",
                                "Once Upon A Time",
                                "Blue Box",
                                "Pool",
                                "Blue Jay"
                            }},
                {"Grey", new []
                            {
                                "Grey",
                                "Slate Grey",
                                "Mid Grey",
                                "Dove Grey",
                                "Smoke",
                                "Charcoal",
                                "Moonlight Waltz"
                            }},
                {"Brown", new []
                            {
                                "Brown",
                                "Stone",
                                "Rum Pink",
                                "Espresso",
                                "Chocolate",
                                "Mocha",
                                "Linen"
                            }},
                {"Red", new []
                        {
                            "Red",
                            "Claret",
                            "Berry",
                            "Mahogany",
                            "Cherry"
                        }},
                            
                {"Orange", new []
                            {
                                "Orange",
                                "Persimmon",
                                "Burnt Orange",
                                "Tangelo",
                                "Salmon"
                            }},
                {"Cream", new []
                            {
                                "Cream",
                                "Ivory",
                                "Champagne"
                            }},
                {"Yellow", new []
                            {
                                "Yellow",
                                "Sunshine",
                                "Canary",
                                "Maize",
                                "Butter Cream"
                            }},
                {"Green", new []
                            {
                                "Green",
                                "Pistachio",
                                "Emerald",
                                "Shamrock",
                                "Forest",
                                "Clover",
                                "Sage",
                                "Sea Mist"
                            }},
                {"Teal", new []
                            {
                                "Teal",
                                "Tealness",
                                "Jade",
                                "Beyond The Sea",
                                "Aqua"
                            }},
                {"Purple", new []
                            {
                                "Purple",
                                "Eggplant",
                                "Viola",
                                "Victorian Lilac",
                                "Royal Bloom",
                                "Grape",
                                "Violet",
                                "Purple Storm",
                                "Peri",
                                "Lilac",
                                "Lavender"
                            }},
                {"Pink", new []
                            {
                                "Pink",
                                "Pomegranate",
                                "Coral",
                                "Loves First Blush",
                                "Fuschia",
                                "Petunia",
                                "Tea Rose"
                            }}
            };

        public const string GroomGoesFreeTitle = "https://aagroom.suitupguys.co.uk/Home/GroomGoesFree";
        public const string BookTryOnTitle = "Book Your Free Try On - Alfred Angelo";
        public const string AccountTitle = "Build Your Outfit - SortMySuit";
        public const string OutFitBuilderTitle = "Build Your Outfit - SortMySuit";
        public const string InspireMeTitle = "Build Your Outfit - SortMySuit";
        public const string HowItWorksTitle = "https://aagroom.suitupguys.co.uk/Help/HowItWorks";
        public const string SubscribeTitle = "Alfred Angelo Groom Suit Up Subscribers";
        public const string TermsConditionsTitle = "https://aagroom.suitupguys.co.uk/Home/TermsConditions";
        public const string CookiesPrivacyTitle = "https://aagroom.suitupguys.co.uk/Home/CookiesPrivacy";
        public const string HelpFAQsTitle = "https://aagroom.suitupguys.co.uk/Help/HelpFAQs";
        public const string PriceListTitle = "https://aagroom.suitupguys.co.uk/Help/PriceList";
        public const string XedoTitle = "Home";

    }
}
